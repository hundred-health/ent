package com.theus.ent.main.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.theus.ent.base.common.constants.DictConstants;
import com.theus.ent.base.config.jwt.JwtToken;
import com.theus.ent.base.model.vo.SysUserVO;
import com.theus.ent.base.service.system.SysUserService;
import com.theus.ent.base.util.ShiroUtils;
import com.theus.ent.core.exception.BusinessException;
import com.theus.ent.main.constants.BusTopicConstants;
import com.theus.ent.main.mapper.BusTopicsMapper;
import com.theus.ent.main.model.dto.bus.BusTopicsAddDTO;
import com.theus.ent.main.model.dto.bus.BusTopicsUpdateDTO;
import com.theus.ent.main.model.dto.bus.FindBusTopicsDTO;
import com.theus.ent.main.model.po.bus.BusTopics;
import com.theus.ent.main.service.BusTopicPatientService;
import com.theus.ent.main.service.BusTopicUserService;
import com.theus.ent.main.service.BusTopicsService;
import com.theus.ent.main.util.UUIDUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author wangzhen
 * @date 2021/7/9 21:43
 * @Description:
 */
@Service
public class BusTopicsServiceImpl extends ServiceImpl<BusTopicsMapper, BusTopics> implements BusTopicsService {

    @Autowired
    private BusTopicPatientService busTopicPatientService;

    @Autowired
    private BusTopicUserService busTopicUserService;

    @Autowired
    private SysUserService sysUserService;



    @Override
    public void add(BusTopicsAddDTO addDTO) {

        BusTopics busTopics = new BusTopics();

        BeanUtils.copyProperties(addDTO,busTopics);

        SysUserVO currentUser = sysUserService.getCurrentUser();

        busTopics.setCreateUserId(currentUser.getId());

        busTopics.setCreateUserName(currentUser.getRealName());

        busTopics.setDelFlag(DictConstants.DelFlag.NORMAL.getValue());

        busTopics.setTopicCode(UUIDUtil.getUUID());

        this.save(busTopics);

    }

    @Override
    public void update(String topicCode, BusTopicsUpdateDTO updateDTO) {
        BusTopics busTopics = this.getById(topicCode);
        if (busTopics == null) {
            throw BusinessException.fail(
                    String.format("更新失败，不存在CODE为 %s 的科研主题", topicCode));
        }
        BeanUtils.copyProperties(updateDTO, busTopics);

        try {
            this.updateById(busTopics);

        } catch (BusinessException e) {
            throw BusinessException.fail(e.getMsg(), e);
        } catch (Exception e) {
            throw BusinessException.fail("科研主题信息更新失败", e);
        }
    }

    @Override
    public IPage<BusTopics> findPage(FindBusTopicsDTO findBusTopicsDTO) {
        IPage<BusTopics> busTopicsPage = new Page<>(findBusTopicsDTO.getPageNum(),
                findBusTopicsDTO.getPageSize());

        busTopicsPage.setRecords(this.baseMapper.findPage(busTopicsPage, findBusTopicsDTO));
        busTopicsPage.getRecords().forEach(busTopics -> {
            //查找主题关联的患者
            busTopics.setPatientNum(busTopicPatientService.countByTopicCode(busTopics.getTopicCode()));

            //主题状态
            busTopics.setTopicState(BusTopicConstants.Status.getName(busTopics.getTopicState()));

            //主题关联的医生数量
            busTopics.setDoctorNum(busTopicUserService.countByTopicCode(busTopics.getTopicCode()));

            busTopics.setDoctorName(sysUserService.findUserById(busTopics.getDocCode(),false).getRealName());
        });
        return busTopicsPage;
    }

    @Override
    public void delete(String topicCode) {

        BusTopics busTopics = this.getById(topicCode);
        if (busTopics == null) {
            throw BusinessException.fail(
                    String.format("更新失败，不存在CODE为 %s 的科研主题", topicCode));
        }

        busTopics.setDelFlag(DictConstants.DelFlag.DELETED.getValue());

        try {
            this.updateById(busTopics);

        } catch (BusinessException e) {
            throw BusinessException.fail(e.getMsg(), e);
        } catch (Exception e) {
            throw BusinessException.fail("科研主题信息删除失败", e);
        }
    }
}

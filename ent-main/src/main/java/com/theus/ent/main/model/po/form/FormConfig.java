package com.theus.ent.main.model.po.form;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @author tangwei
 * @date 2021-07-04 18:52
 */
@Data
public class FormConfig {
    /**
     * 表名
     */
    @TableId
    private String id;

    /**
     * 表名
     */
    private String tableName;

    /**
     * 列名
     */
    private String columnName;

    /**
     * 是否隐藏（0 否 1 是）
     */
    private String isHide;

    /**
     * 是否动态列（0 否 1 是）
     */
    private String isDynamic;

    /**
     * json串（isDynamic为1时必填）
     */
    private String json;

    /**
     * 是否启用（0 否 1 是）
     */
    private String status;
}

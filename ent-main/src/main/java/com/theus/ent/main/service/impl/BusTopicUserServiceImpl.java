package com.theus.ent.main.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.theus.ent.base.model.po.system.SysUser;
import com.theus.ent.base.service.system.SysDictService;
import com.theus.ent.base.service.system.SysUserService;
import com.theus.ent.core.exception.BusinessException;
import com.theus.ent.main.mapper.BusTopicUserMapper;
import com.theus.ent.main.model.dto.bus.BusTopicUserAddDTO;
import com.theus.ent.main.model.dto.bus.DelBusTopicUserDTO;
import com.theus.ent.main.model.dto.bus.FindBusTopicUserDTO;
import com.theus.ent.main.model.po.bus.BusTopicUser;
import com.theus.ent.main.model.vo.bus.BusTopicUserVo;
import com.theus.ent.main.service.BusTopicUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author wangzhen
 * @date 2021/7/10 16:04
 * @Description:
 */
@Service
public class BusTopicUserServiceImpl extends ServiceImpl<BusTopicUserMapper, BusTopicUser> implements BusTopicUserService {

    @Autowired
    private BusTopicUserMapper busTopicUserMapper;

    @Autowired
    private SysDictService sysDictService;

    @Autowired
    private SysUserService sysUserService;

    @Override
    public void add(BusTopicUserAddDTO addDTO) {

        BusTopicUser busTopicUser = new BusTopicUser();

        BeanUtils.copyProperties(addDTO,busTopicUser);

        this.save(busTopicUser);
    }

    @Override
    public List<BusTopicUserVo> findByBusTopic(FindBusTopicUserDTO findBusTopicUserDTO) {
        QueryWrapper<BusTopicUser> busTopicPatientQueryWrapper = new QueryWrapper<>();
        busTopicPatientQueryWrapper.eq("topic_code",findBusTopicUserDTO.getTopicCode());
        List<BusTopicUser> busTopicUserList = busTopicUserMapper.selectList(busTopicPatientQueryWrapper);

        List<BusTopicUserVo> busTopicUserVoList = busTopicUserList.stream().map(busTopicUser -> {

            BusTopicUserVo busTopicUserVo = new BusTopicUserVo();

            SysUser sysUser = sysUserService.getById(busTopicUser.getDoctorCode());
            busTopicUserVo.setCode(busTopicUser.getCode());
            busTopicUserVo.setRealName(sysUser.getRealName());
            busTopicUserVo.setDeptName(this.getItemName("用户科室",sysUser.getDeptId()));

            busTopicUserVo.setTelephone(sysUser.getTelephone());

            busTopicUserVo.setTitle(this.getItemName("用户头衔",sysUser.getDeptId()));

            return busTopicUserVo;
        }).collect(Collectors.toList());

        if(StrUtil.isNotBlank(findBusTopicUserDTO.getDoctorName())) {

            busTopicUserVoList = busTopicUserVoList.stream().filter(busTopicUserVo -> busTopicUserVo.getRealName().contains(findBusTopicUserDTO.getDoctorName())
            ).collect(Collectors.toList());
        }

        return busTopicUserVoList;
    }

    @Override
    public int countByTopicCode(String topicCode) {
        QueryWrapper<BusTopicUser> busTopicPatientQueryWrapper = new QueryWrapper<>();
        busTopicPatientQueryWrapper.eq("topic_code",topicCode);
        return busTopicUserMapper.selectCount(busTopicPatientQueryWrapper);
    }

    @Override
    public void deleteByCode(DelBusTopicUserDTO delBusTopicUserDTO) {

        for(String code : delBusTopicUserDTO.getCodes()) {

            BusTopicUser busTopicUser = this.getById(code);

            if (busTopicUser == null) {

                continue;
            }

            try {
                this.removeById(code);

            } catch (BusinessException e) {
                throw BusinessException.fail(e.getMsg(), e);
            } catch (Exception e) {
                throw BusinessException.fail("科研主题关联医生删除失败", e);
            }
        }

    }

    @Override
    public String getItemName(String name, String itemValue) {
        return busTopicUserMapper.getItemName(name,itemValue);
    }

    @Override
    public void addAll(List<BusTopicUserAddDTO> busTopicUserAddDTOs) {

        for(BusTopicUserAddDTO addDTO : busTopicUserAddDTOs) {

            QueryWrapper<BusTopicUser> busTopicPatientQueryWrapper = new QueryWrapper<>();

            busTopicPatientQueryWrapper.eq("topic_code",addDTO.getTopicCode());

            busTopicPatientQueryWrapper.eq("doctor_code",addDTO.getDoctorCode());

            Integer selectCount = busTopicUserMapper.selectCount(busTopicPatientQueryWrapper);

            if(selectCount >=1) {

                continue;
            }

            this.add(addDTO);
        }
    }
}

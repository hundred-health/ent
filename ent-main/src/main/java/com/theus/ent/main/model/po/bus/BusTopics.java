package com.theus.ent.main.model.po.bus;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.Date;

/**
 * @author wangzhen
 * @date 2021/7/8 22:21
 * @Description: 科研主题
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BusTopics implements Serializable {

    @TableId
    private String  topicCode;

    private String topicName;

    private String docCode;

    private String topicState;

    private Date createDate = new Date();

    private Date endDate;

    private Integer delFlag; //0：正常，1：删除

    private String createUserId;

    private String createUserName;

    @TableField(exist = false)
    private String doctorName;

    @TableField(exist = false)
    private Integer doctorNum;

    @TableField(exist = false)
    private Integer patientNum;
}

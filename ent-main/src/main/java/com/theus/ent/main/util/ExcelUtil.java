package com.theus.ent.main.util;

import com.theus.ent.core.exception.BusinessException;
import io.netty.util.internal.StringUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.*;

/**
 * excel工具类
 *
 * @author fangkun
 */
public class ExcelUtil {
    /**
     * 读取Excel的内容，转换成指定类型的List
     * 1、解析类，列出需要从Excel读取的字段集合
     * 2、读取Excel数据集合，遍历行
     * 3、子循环遍历需要读取的字段集合并赋值到实体类
     *
     * @param file excel文件
     * @param cls  模型类
     * @param <E>
     * @return
     */
    public static <E> List<E> readExcel(MultipartFile file, Class<E> cls) throws IOException {
        List<E> resultList = new ArrayList<>(20);
        InputStream inputStream = file.getInputStream();
        XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
        //判断页签是否大于0
        if (workbook.getNumberOfSheets() <= 0) {
            return Collections.EMPTY_LIST;
        }
        //只处理第一个页签
        XSSFSheet sheet = workbook.getSheetAt(0);
        if (sheet.getPhysicalNumberOfRows() <= 0) {
            return Collections.EMPTY_LIST;
        }
        //获得表头
        Row rowHead = sheet.getRow(0);

        //根据不同的data放置不同的表头
        Map<String, Integer> headMap = new HashMap(10);
        Map<String, ExcelImportField> fieldMap = new HashMap(10);

        List<Field> allFields = Arrays.asList(cls.getDeclaredFields());
        List<Field> excelFields = new ArrayList<>();
        for (Field field : allFields) {
            if (field.isAnnotationPresent(ExcelImportField.class)) {
                //将有注解的类属性字段名添加到表格列集合，排除不需要读取的Excel字段
                field.setAccessible(true);
                excelFields.add(field);
                ExcelImportField fieldAttr = field.getAnnotation(ExcelImportField.class);
                for (int i = 0; i < rowHead.getPhysicalNumberOfCells(); i++) {
                    if (fieldAttr.title().equals(rowHead.getCell(i).getStringCellValue())) {
                        headMap.put(field.getName(), i);
                        fieldMap.put(field.getName(), fieldAttr);
                    }
                }
                if (!headMap.containsKey(field.getName())) {
                    throw BusinessException.fail( "在Excel文件中没有找到列[" + fieldAttr.title() + "]，请检查！");
                }
            }
        }

        int totalRowNum = sheet.getLastRowNum();

        if (totalRowNum <= 0) {
            return Collections.EMPTY_LIST;
        }

        Map<String, List<Object>> filedValuesMap = new HashMap<>(10);
        for (int i = 1; i <= totalRowNum; i++) {
            XSSFRow row = sheet.getRow(i);
            if (row == null) {
                continue;
            }
            boolean isBlankRow = true;
            for (int j = 0; j < row.getLastCellNum(); j++) {
                System.out.println("行" + i + ":" + rowHead.getCell(j).getStringCellValue());
                Cell cell = row.getCell(j);
                if (cell == null) {
                    continue;
                }
                if (CellType.BLANK.equals(row.getCell(j).getCellType())) {
                    continue;
                } else if (CellType.STRING.equals(row.getCell(j).getCellType()) && StringUtil.isNullOrEmpty(cell.getStringCellValue().trim())) {
                    continue;
                } else {
                    isBlankRow = false;
                    break;
                }
            }
            if (isBlankRow) {
                continue;
            }
            try {
                E object = cls.newInstance();
                for (Field field : excelFields) {
                    int cellIndex = headMap.get(field.getName());
                    XSSFCell cell = row.getCell(cellIndex);
                    ExcelImportField importField = fieldMap.get(field.getName());
                    if (cell != null && !CellType.BLANK.equals(cell.getCellType())) {
                                try {
                                    Class fieldType = field.getType();
                                    if (fieldType.isAssignableFrom(String.class)) {
                                        field.set(object, getStringCellValue(cell));
                                    } else if (fieldType.isAssignableFrom(Integer.class)) {
                                Double value = cell.getNumericCellValue();
                                field.set(object, value.intValue());
                            } else if (fieldType.isAssignableFrom(Short.class)) {
                                field.setShort(object, Short.valueOf(cell.getStringCellValue()));
                            } else if (fieldType.isAssignableFrom(Float.class)) {
                                field.setFloat(object, Float.valueOf(cell.getStringCellValue()));
                            } else if (fieldType.isAssignableFrom(Double.class)) {
                                field.setDouble(object, cell.getNumericCellValue());
                            } else if (fieldType.isAssignableFrom(BigDecimal.class)) {
                                double numericCellValue = cell.getNumericCellValue();
                                field.set(object, BigDecimal.valueOf(numericCellValue));
                            } else if (fieldType.isAssignableFrom(Boolean.class)) {
                                field.setBoolean(object, cell.getBooleanCellValue());
                            } else if (fieldType.isAssignableFrom(Date.class)) {
                                field.set(object, cell.getDateCellValue());
                            }
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                            throw BusinessException.fail("Excel文件第" + i + "行(不包括表头)数据的[" + importField.title() + "]格式不正确，无法导入，请检查！");
                        }
                    }
                    if (field.get(object) == null && importField.isRequired()) {
                        throw BusinessException.fail( "Excel文件第" + i + "行(不包括表头)数据的[" + importField.title() + "]为空，无法导入，请检查！");
                    }
                    if (field.get(object) != null && !"".equals(String.valueOf(field.get(object))) && importField.isUnique()) {
                        if (filedValuesMap.containsKey(field.getName())) {
                            List<Object> objectList = filedValuesMap.get(field.getName());
                            if (objectList.contains(field.get(object))) {
                                throw BusinessException.fail("Excel文件中列[" + importField.title() + "]有重复值[" + field.get(object) + "]，无法导入，请检查！");
                            } else {
                                objectList.add(field.get(object));
                            }
                        } else {
                            List<Object> objectList = new ArrayList<>();
                            objectList.add(field.get(object));
                            filedValuesMap.put(field.getName(), objectList);
                        }
                    }
                }
                resultList.add(object);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }

        return resultList;
    }

    private static String getStringCellValue(Cell cell) {
        switch (cell.getCellType()) {
            case FORMULA:
                return cell.getCellFormula();
            case NUMERIC:
                return String.valueOf(cell.getNumericCellValue());
            case STRING:
                return cell.getStringCellValue();
            case BOOLEAN:
                return String.valueOf(cell.getBooleanCellValue());
            case ERROR:
                return "ERROR";
            default:
                return null;
        }
    }
}

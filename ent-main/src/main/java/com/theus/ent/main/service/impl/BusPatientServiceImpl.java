package com.theus.ent.main.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.theus.ent.base.model.vo.SysUserVO;
import com.theus.ent.base.service.system.SysUserService;
import com.theus.ent.core.exception.BusinessException;
import com.theus.ent.main.constants.BusPatientConstants;
import com.theus.ent.main.mapper.BusPatientMapper;
import com.theus.ent.main.model.dto.bus.BusPatientDTO;
import com.theus.ent.main.model.dto.bus.PageQueryDTO;
import com.theus.ent.main.model.po.bus.BusPatient;
import com.theus.ent.main.model.vo.bus.BusPatientVO;
import com.theus.ent.main.service.BusPatientService;
import com.theus.ent.main.util.EntBeanUtils;
import com.theus.ent.main.util.ExcelUtil;
import io.netty.util.internal.StringUtil;
import org.apache.commons.collections.CollectionUtils;
import org.omg.CORBA.portable.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author fangkun
 * @since 2021-07-12
 */
@Service
public class BusPatientServiceImpl extends ServiceImpl<BusPatientMapper, BusPatient> implements BusPatientService {
    public final Logger LOGGER = LoggerFactory.getLogger(BusPatientServiceImpl.class);

    @Autowired
    private BusPatientMapper mapper;

    @Autowired
    SysUserService sysUserService;

    @Override
    public void savePatient(BusPatientDTO dto) {
        BusPatient entity = new BusPatient();
        BeanUtils.copyProperties(dto, entity);
        //如果修改就需要查询当前登陆人
        if (StringUtils.isEmpty(dto.getPatientCode())) {
            SysUserVO user = sysUserService.getCurrentUser();
            entity.setCreateUserId(user.getId());
            entity.setCreateUserName(user.getRealName());
        }
        //保存
        saveOrUpdate(entity);
    }

    @Override
    public BusPatientVO load(String patientCode) {
        //查询患者信息
        BusPatient patient = getById(patientCode);
        if (patient == null) {
            return null;
        }

        BusPatientVO vo = new BusPatientVO();
        EntBeanUtils.copyProperties(patient, vo);
        return vo;
    }

    @Override
    public void delete(String patientCode) {
        removeById(patientCode);
    }

    @Override
    public IPage<BusPatientVO> pageQuery(PageQueryDTO dto) {
        IPage<BusPatient> page = new Page<>(dto.getPageNum(),
                dto.getPageSize());

        //分页查询患者
        List<BusPatient> patients = mapper.pageQuery(page, dto);

        IPage<BusPatientVO> pageVO = new Page<>();
        BeanUtils.copyProperties(page, pageVO);

        //如果没有数据 直接返回
        if (CollectionUtils.isEmpty(patients)) {
            return pageVO;
        }

        //如果有数据返回则将返回数据转成Vo
        List<BusPatientVO> list = new ArrayList<>();
        patients.forEach(e -> {
            BusPatientVO vo = new BusPatientVO();
            EntBeanUtils.copyProperties(e, vo);
            list.add(vo);
        });

        // 往分页添加数据
        pageVO.setRecords(list);

        return pageVO;
    }

    @Override
    public void importTemplate(MultipartFile file) {
        if (!file.getOriginalFilename().matches(BusPatientConstants.EXCELSX_FILE_REGEX)) {
            throw BusinessException.fail("上传文件格式不正确！");
        }
        List<BusPatient> entityList = new ArrayList<>();
        try {
            //Excel 转换成List
            List<BusPatientDTO> list = ExcelUtil.readExcel(file, BusPatientDTO.class);
            if (CollectionUtils.isEmpty(list)) {
                throw BusinessException.fail("Excel数据为空，请检查！");
            }

            //将数据存表

            list.forEach(e->{
                BusPatient entity=new BusPatient();
                BeanUtils.copyProperties(e,entity);
                //获取当前用户
                SysUserVO currentUser = sysUserService.getCurrentUser();
                entity.setCreateUserId(currentUser.getId());
                entity.setCreateUserName(currentUser.getRealName());
                entityList.add(entity);
            });
        } catch (IOException e) {
            LOGGER.error("excel解析错误:{}", e);
            throw BusinessException.fail("读取文件失败！");
        }

        //批量保存
        saveBatch(entityList);
    }
}

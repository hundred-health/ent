package com.theus.ent.main.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.theus.ent.main.model.po.bus.BusTopicPatient;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;


@Mapper
@Repository
public interface BusTopicPatientMapper extends BaseMapper<BusTopicPatient> {


}

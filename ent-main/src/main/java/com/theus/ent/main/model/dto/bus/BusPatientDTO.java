package com.theus.ent.main.model.dto.bus;

import com.theus.ent.main.util.ExcelImportField;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class BusPatientDTO {
    /**
     * 患者编号
     */
    private String patientCode;
    /**
     * 实验编号
     */
    private String testCode;
    /**
     * 患者姓名
     */
    @ExcelImportField(title = "患者姓名")
    private String patientName;
    /**
     * 联系方式
     */
    @ExcelImportField(title = "联系方式")
    private String phone;
    /**
     * 证件号
     */
    @ExcelImportField(title = "身份证号")
    private String cardNo;
    /**
     * 出生日期
     */
    @ExcelImportField(title = "出生日期")
    private Date birthday;
    /**
     * 性别
     */
    @ExcelImportField(title = "性别")
    private String gender;
    /**
     * 现住址
     */
    @ExcelImportField(title = "现住址")
    private String address;
    /**
     * 身高
     */
    private BigDecimal height;
    /**
     * 体重
     */
    private BigDecimal weight;
    /**
     * 门诊号
     */
    private String outpatientNo;
    /**
     * 住院号
     */
    private String inpatientNo;
    /**
     * 是否签名
     */
    private String signState;

    private String createUserId;

    private String createUserName;
}

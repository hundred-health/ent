package com.theus.ent.main.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.theus.ent.base.common.constants.DictConstants;
import com.theus.ent.core.exception.BusinessException;
import com.theus.ent.main.mapper.BusPatientTableMapper;
import com.theus.ent.main.model.dto.bus.BusPatientTableAddDTO;
import com.theus.ent.main.model.dto.bus.BusPatientTableUpdateDTO;
import com.theus.ent.main.model.dto.bus.FindBusPatientTableDTO;
import com.theus.ent.main.model.po.bus.BusPatientTable;
import com.theus.ent.main.service.BusPatientTableService;
import com.theus.ent.main.util.UUIDUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author wangzhen
 * @date 2021/7/9 21:15
 * @Description:
 */
@Service
public class BusPatientTableServiceImpl extends ServiceImpl<BusPatientTableMapper, BusPatientTable> implements BusPatientTableService {

    @Autowired
    private BusPatientTableMapper busPatientTableMapper;

    @Override
    public void add(BusPatientTableAddDTO addDTO) {

        BusPatientTable busPatientTable = new BusPatientTable();

        BeanUtils.copyProperties(addDTO,busPatientTable);

        busPatientTable.setDelFlag(DictConstants.DelFlag.NORMAL.getValue());

        busPatientTable.setTableCode(UUIDUtil.getUUID());

        this.save(busPatientTable);
    }

    @Override
    public void update(String tableCode, BusPatientTableUpdateDTO updateDTO) {
        BusPatientTable busPatientTable = this.getById(tableCode);
        if (busPatientTable == null) {
            throw BusinessException.fail(
                    String.format("更新失败，不存在CODE为 %s 的表单", tableCode));
        }
        BeanUtils.copyProperties(updateDTO, busPatientTable);

        try {
            this.updateById(busPatientTable);

        } catch (BusinessException e) {
            throw BusinessException.fail(e.getMsg(), e);
        } catch (Exception e) {
            throw BusinessException.fail("表单信息更新失败", e);
        }
    }

    @Override
    public IPage<BusPatientTable> findPage(FindBusPatientTableDTO findBusPatientTableDTO) {
        IPage<BusPatientTable> busPatientTableIPage = new Page<>(findBusPatientTableDTO.getPageNum(),
                findBusPatientTableDTO.getPageSize());

        List<BusPatientTable> busPatientTableList = this.busPatientTableMapper.findPage(busPatientTableIPage, findBusPatientTableDTO);

        busPatientTableIPage.setRecords(busPatientTableList);

        return busPatientTableIPage;

    }

    @Override
    public void delete(String tableCode) {
        BusPatientTable busPatientTable = this.getById(tableCode);
        if (busPatientTable == null) {
            throw BusinessException.fail(
                    String.format("删除失败，不存在CODE为 %s 的表单", tableCode));
        }
        busPatientTable.setDelFlag(DictConstants.DelFlag.DELETED.getValue());
        try {
            this.updateById(busPatientTable);

        } catch (BusinessException e) {
            throw BusinessException.fail(e.getMsg(), e);
        } catch (Exception e) {
            throw BusinessException.fail("表单信息删除失败", e);
        }
    }
}

package com.theus.ent.main.model.vo.bus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wangzhen
 * @date 2021/7/11 22:12
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BusTopicPatientVo {

    private String code;

    private String topicCode;

    private String patientCode; //患者编号

    private String patientName; //患者姓名

    private String phone; //联系方式

    private String cardNo; //身份证号
}

package com.theus.ent.main.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.theus.ent.main.model.dto.bus.BusTopicPatientAddDTO;
import com.theus.ent.main.model.dto.bus.DelBusTopicPatientDTO;
import com.theus.ent.main.model.dto.bus.FindBusTopicPatientDTO;
import com.theus.ent.main.model.po.bus.BusTopicPatient;
import com.theus.ent.main.model.vo.bus.BusTopicPatientVo;

import java.util.List;

/**
 * @author wangzhen
 * @date 2021/7/9 20:42
 * @Description:
 */
public interface BusTopicPatientService extends IService<BusTopicPatient> {

    void add(BusTopicPatientAddDTO addDTO);

    int countByTopicCode(String topicCode);

    void deleteByCode(DelBusTopicPatientDTO delBusTopicPatientDTO);

    List<BusTopicPatientVo> findByBusTopicCode(FindBusTopicPatientDTO findBusTopicPatientDTO);

    void addAll(List<BusTopicPatientAddDTO> addDTOs);
}

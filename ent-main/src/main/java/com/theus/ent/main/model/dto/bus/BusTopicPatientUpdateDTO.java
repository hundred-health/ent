package com.theus.ent.main.model.dto.bus;

import lombok.Data;

/**
 * @author wangzhen
 * @date 2021/7/10 15:58
 * @Description:
 */
@Data
public class BusTopicPatientUpdateDTO {

    private String tableCode;

    private String topicCode;

    private String patientCode;
}

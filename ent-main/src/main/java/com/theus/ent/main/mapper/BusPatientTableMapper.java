package com.theus.ent.main.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.theus.ent.main.model.dto.bus.FindBusPatientTableDTO;
import com.theus.ent.main.model.po.bus.BusPatientTable;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Mapper
@Repository
public interface BusPatientTableMapper extends BaseMapper<BusPatientTable> {
    /**
     * 查询表单列表
     * @param page 分页
     * @param findBusPatientTableDTO 查询条件
     * @return 表单list
     */
    List<BusPatientTable> findPage(IPage<BusPatientTable> page, @Param("busPatientTable") FindBusPatientTableDTO findBusPatientTableDTO);

}

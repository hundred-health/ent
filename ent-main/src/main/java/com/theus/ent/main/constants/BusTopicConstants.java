package com.theus.ent.main.constants;

/**
 * @author wangzhen
 * @date 2021/7/10 19:53
 * @Description:
 */
public class BusTopicConstants {

    /**
     * 科研主题状态
     */
    public enum Status{

        //未开始
        NOTSTART("01","未开始"),
        //进行中
        DOING("02","进行中"),
        //已终止
        TERMINATED("03","已终止"),
        //已终止
        FINISHED("04","已结束");


        Status(String value, String name) {
            this.value = value;
            this.name = name;
        }

        private final String value;
        private final String name;

        public String getValue() {
            return value;
        }

        public String getName() {
            return name;
        }

        public static String getName(String value) {
            for (Status item : Status.values()) {
                if (item.getValue().equals(value)) {
                    return item.getName();
                }
            }
            return "";
        }
    }

    /**
     * 表单分类
     */
    public enum TableCartegory{

        //首次诊断
        FIRSTDIAGNOSE("01","首次诊断"),
        //入组前访视
        INTERVIEW("02","入组前访视"),
        //入排标准
        STANDARD("03","入排标准"),
        //手术组
        THESURGICAL("04","手术组"),
        //放疗组
        GTRT("05","放疗组"),
        //随访
        FOLLOWUP("06","随访");


        TableCartegory(String value, String name) {
            this.value = value;
            this.name = name;
        }

        private final String value;
        private final String name;

        public String getValue() {
            return value;
        }

        public String getName() {
            return name;
        }

        public static String getName(String value) {
            for (Status item : Status.values()) {
                if (item.getValue().equals(value)) {
                    return item.getName();
                }
            }
            return "";
        }
    }
}

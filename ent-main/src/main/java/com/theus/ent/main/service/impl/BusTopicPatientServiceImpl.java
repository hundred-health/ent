package com.theus.ent.main.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.theus.ent.core.exception.BusinessException;
import com.theus.ent.main.mapper.BusTopicPatientMapper;
import com.theus.ent.main.model.dto.bus.BusTopicPatientAddDTO;

import com.theus.ent.main.model.dto.bus.DelBusTopicPatientDTO;
import com.theus.ent.main.model.dto.bus.FindBusTopicPatientDTO;
import com.theus.ent.main.model.po.bus.BusPatient;
import com.theus.ent.main.model.po.bus.BusTopicPatient;

import com.theus.ent.main.model.vo.bus.BusTopicPatientVo;
import com.theus.ent.main.model.vo.bus.BusTopicUserVo;
import com.theus.ent.main.service.BusPatientService;
import com.theus.ent.main.service.BusTopicPatientService;
import com.theus.ent.main.util.UUIDUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.stream.Collectors;

/**
 * @author wangzhen
 * @date 2021/7/9 21:16
 * @Description:
 */
@Service
public class BusTopicPatientServiceImpl extends ServiceImpl<BusTopicPatientMapper, BusTopicPatient> implements BusTopicPatientService {

    @Autowired
    private BusTopicPatientMapper busTopicPatientMapper;

    @Autowired
    private BusPatientService busPatientService;

    @Override
    public void add(BusTopicPatientAddDTO addDTO) {

        BusTopicPatient busTopicPatient = new BusTopicPatient();

        BeanUtils.copyProperties(addDTO,busTopicPatient);

        busTopicPatient.setCode(UUIDUtil.getUUID());

        this.save(busTopicPatient);
    }

    @Override
    public int countByTopicCode(String topicCode) {
        QueryWrapper<BusTopicPatient> busTopicPatientQueryWrapper = new QueryWrapper<>();
        busTopicPatientQueryWrapper.eq("topic_code",topicCode);
        return busTopicPatientMapper.selectCount(busTopicPatientQueryWrapper);
    }

    @Override
    @Transactional
    public void deleteByCode(DelBusTopicPatientDTO delBusTopicPatientDTO) {

        for(String code : delBusTopicPatientDTO.getCodes()) {

            BusTopicPatient busTopicPatient = this.getById(code);

            if (busTopicPatient == null) {

                continue;
            }

            try {
                this.removeById(code);

            } catch (BusinessException e) {
                throw BusinessException.fail(e.getMsg(), e);
            } catch (Exception e) {
                throw BusinessException.fail("科研主题关联患者删除失败", e);
            }
        }


    }

    @Override
    public List<BusTopicPatientVo> findByBusTopicCode(FindBusTopicPatientDTO findBusTopicPatientDTO) {

        QueryWrapper<BusTopicPatient> busTopicPatientQueryWrapper = new QueryWrapper<>();

        busTopicPatientQueryWrapper.eq("topic_code",findBusTopicPatientDTO.getTopicCode());

        List<BusTopicPatient> busTopicPatientList = busTopicPatientMapper.selectList(busTopicPatientQueryWrapper);

        List<BusTopicPatientVo> busTopicPatientVoList = busTopicPatientList.stream().map(busTopicTable -> {

            BusPatient busPatient = busPatientService.getById(busTopicTable.getPatientCode());

            BusTopicPatientVo busTopicPatientVo = new BusTopicPatientVo();

            busTopicPatientVo.setCode(busTopicTable.getCode());

            busTopicPatientVo.setTopicCode(findBusTopicPatientDTO.getTopicCode());

            busTopicPatientVo.setPatientCode(busPatient.getPatientCode());

            busTopicPatientVo.setCardNo(busPatient.getCardNo());

            busTopicPatientVo.setPatientName(busPatient.getPatientName());

            busTopicPatientVo.setPhone(busPatient.getPhone());

            return busTopicPatientVo;
        }).collect(Collectors.toList());

        if(StrUtil.isNotBlank(findBusTopicPatientDTO.getPatientName())) {

            for(int index =0; index < busTopicPatientVoList.size(); index ++) {

                BusTopicPatientVo busTopicPatientVo = busTopicPatientVoList.get(index);

                if(!busTopicPatientVo.getPatientName().contains(findBusTopicPatientDTO.getPatientName())) {

                    busTopicPatientVoList.remove(index);
                }
            }

        }

        return busTopicPatientVoList;
    }

    @Override
    public void addAll(List<BusTopicPatientAddDTO> addDTOs) {

        for(BusTopicPatientAddDTO addDTO : addDTOs) {
            QueryWrapper<BusTopicPatient> busTopicPatientQueryWrapper = new QueryWrapper<>();

            busTopicPatientQueryWrapper.eq("topic_code",addDTO.getTopicCode());

            busTopicPatientQueryWrapper.eq("patient_code",addDTO.getPatientCode());

            Integer selectCount = busTopicPatientMapper.selectCount(busTopicPatientQueryWrapper);

            if(selectCount >=1) {

                continue;
            }

            this.add(addDTO);
        }
    }
}

package com.theus.ent.main.model.po.bus;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;

import java.io.Serializable;

/**
 * @author wangzhen
 * @date 2021/7/10 16:20
 * @Description:
 */
@Data
public class BusPatient implements Serializable {
    @TableId
    private String patientCode; //患者编号

    private String testCode; //受试者编号

    private String patientName; //患者姓名

    private String phone; //联系方式

    private String cardNo; //身份证号

    private String birthday; //出生日期

    private String gender; //性别

    private String address; //现住地址

    private String height; //身高

    private String weight; //体重

    private String outpatientNo; //门诊号

    private String inpatientNo; //住院号

    private String signState; //签署知情同意书

    @TableLogic
    private Integer delFlag;

    private String createUserId;

    private String createUserName;
}

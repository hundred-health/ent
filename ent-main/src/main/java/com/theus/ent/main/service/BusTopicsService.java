package com.theus.ent.main.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.theus.ent.main.model.dto.bus.BusTopicsAddDTO;
import com.theus.ent.main.model.dto.bus.BusTopicsUpdateDTO;
import com.theus.ent.main.model.dto.bus.FindBusTopicsDTO;
import com.theus.ent.main.model.po.bus.BusTopics;

/**
 * @author wangzhen
 * @date 2021/7/9 20:42
 * @Description:
 */
public interface BusTopicsService extends IService<BusTopics> {

    /**
     * 添加科研主题
     * @param addDTO
     */
    void add(BusTopicsAddDTO addDTO);

    /**
     * 更新科研主题
     * @param topicCode
     * @param updateDTO
     */
    void update(String topicCode, BusTopicsUpdateDTO updateDTO);


    /**
     * 查询科研主题（分页）
     * @param findBusTopicsDTO 过滤条件
     * @return RequestResult
     */
    IPage<BusTopics> findPage(FindBusTopicsDTO findBusTopicsDTO);

    /**
     * 根据code删除科研主题
     * @param topicCode
     */
    void delete(String topicCode);
}

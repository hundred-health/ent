package com.theus.ent.main.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.theus.ent.main.model.dto.bus.FindBusTopicsDTO;
import com.theus.ent.main.model.po.bus.BusTopics;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Mapper
@Repository
public interface BusTopicsMapper extends BaseMapper<BusTopics> {

    /**
     * 查询科研主题列表
     * @param page 分页
     * @param findBusTopicsDTO 查询条件
     * @return 科研主题list
     */
    List<BusTopics> findPage(IPage<BusTopics> page, @Param("busTopics") FindBusTopicsDTO findBusTopicsDTO);
}

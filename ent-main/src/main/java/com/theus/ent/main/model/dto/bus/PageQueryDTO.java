package com.theus.ent.main.model.dto.bus;

import com.theus.ent.base.model.dto.BaseSplitPageDTO;
import lombok.Data;

@Data
public class PageQueryDTO extends BaseSplitPageDTO {
    /**
     * 患者姓名
     */
    private String patientName;
    /**
     * 联系方式
     */
    private String phone;
    /**
     * 身份证号
     */
    private String cardNo;
}

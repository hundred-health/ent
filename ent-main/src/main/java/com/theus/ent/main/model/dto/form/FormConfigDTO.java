package com.theus.ent.main.model.dto.form;

import lombok.Data;

/**
 * @author tangwei
 * @date 2021-07-04 18:52
 */
@Data
public class FormConfigDTO {
    /**
     * 列的名称。在对象内是唯一的
     */
    private String name;

    /**
     * 列的ID。在对象内是唯一的。列ID可能不是顺序的
     */
    private Integer columnId;

    /**
     * 列的最大长度（以字节为单位）
     */
    private Short maxLength;

    /**
     * 如果列是数字，列的精度; 否则，0
     */
    private Short precision;

    /**
     * 如果列是数字,列的比例， 否则，0。
     */
    private Short scale;

    /**
     * 1 =列可以为空
     */
    private Boolean isNullable;

    /**
     * 数据类型 关联sys.types
     */
    private String dataType;

    /**
     * 列描述 关联sys.extended_properties
     */
    private String columnDesc;

    /**
     * 配置id（关联form_config）
     */
    private String id;

    /**
     * 是否隐藏（0 否 1 是）（关联form_config）
     */
    private String isHide;

    /**
     * 是否动态列（0 否 1 是）（关联form_config）
     */
    private String isDynamic;

    /**
     * json串（isDynamic为1时必填）（关联form_config）
     */
    private String json;

    /**
     * 是否启用（0 否 1 是）（关联form_config）
     */
    private String status;
}

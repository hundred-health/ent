package com.theus.ent.main.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.theus.ent.main.model.dto.bus.PageQueryDTO;
import com.theus.ent.main.model.po.bus.BusPatient;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author fangkun
 * @since 2021-07-12
 */
public interface BusPatientMapper extends BaseMapper<BusPatient> {
    /**
     * 分页查询患者信息
     *
     * @param dto 分页参数
     * @return
     */
    List<BusPatient> pageQuery(IPage<BusPatient> page, @Param("patient") PageQueryDTO dto);
}

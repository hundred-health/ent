package com.theus.ent.main.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.theus.ent.base.model.po.system.SysUser;
import com.theus.ent.main.model.po.bus.BusTopicUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Mapper
@Repository
public interface BusTopicUserMapper extends BaseMapper<BusTopicUser> {

    String getItemName(@Param("name") String name, @Param("itemValue") String itemValue);

}

package com.theus.ent.main.controller;

import com.theus.ent.base.common.annotation.SysLogs;
import com.theus.ent.base.model.po.system.SysUser;
import com.theus.ent.core.bean.ResponseCode;
import com.theus.ent.core.bean.ResponseResult;
import com.theus.ent.main.model.dto.bus.BusTopicPatientAddDTO;
import com.theus.ent.main.model.dto.bus.BusTopicUserAddDTO;
import com.theus.ent.main.model.dto.bus.DelBusTopicUserDTO;
import com.theus.ent.main.model.dto.bus.FindBusTopicUserDTO;
import com.theus.ent.main.model.vo.bus.BusTopicUserVo;
import com.theus.ent.main.service.BusTopicUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author wangzhen
 * @date 2021/7/10 16:05
 * @Description:
 */
@Api(tags = "科研医生")
@RestController
@RequestMapping("/bus/topic/user")
public class BusTopicUserController {

    @Resource
    private BusTopicUserService busTopicUserService;

    @PostMapping(value = {"/get"})
    @ApiOperation(value = "根据科研code获取关联的医生信息")
    @SysLogs("根据科研code获取关联的医生信息")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<List<BusTopicUserVo>> getUser(@RequestBody @Validated @ApiParam(value = "科研主题医生") FindBusTopicUserDTO findBusTopicUserDTO) {
        return ResponseResult.e(ResponseCode.OK, busTopicUserService.findByBusTopic(findBusTopicUserDTO));
    }


    @PostMapping(value = {"/add"})
    @ApiOperation(value = "添加科研主题医生")
    @SysLogs("添加科研主题医生")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<String> add(@RequestBody @Validated @ApiParam(value = "科研主题医生") List<BusTopicUserAddDTO> busTopicUserAddDTOs) {
        busTopicUserService.addAll(busTopicUserAddDTOs);
        return ResponseResult.e(ResponseCode.OK);
    }

    @PostMapping(value = {"/delete"})
    @ApiOperation(value = "删除科研主题医生")
    @SysLogs("删除科研主题医生")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<String> delete(@RequestBody @Validated @ApiParam(value = "删除可以主题医生") DelBusTopicUserDTO delBusTopicUserDTO) {

        busTopicUserService.deleteByCode(delBusTopicUserDTO);

        return ResponseResult.e(ResponseCode.OK);
    }

}

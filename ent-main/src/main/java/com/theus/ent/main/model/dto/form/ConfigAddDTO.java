package com.theus.ent.main.model.dto.form;

import lombok.Data;

/**
 * @author tangwei
 * @date 2021-07-10 20:42
 */
@Data
public class ConfigAddDTO {

    /**
     * 表名
     */
    private String tableName;

    /**
     * 列名
     */
    private String columnName;

    /**
     * 是否隐藏（0 否 1 是）
     */
    private String isHide;

    /**
     * json串（isDynamic为1时必填）
     */
    private ConfigJsonDTO json;
}

package com.theus.ent.main.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.theus.ent.base.common.annotation.SysLogs;
import com.theus.ent.core.bean.ResponseCode;
import com.theus.ent.core.bean.ResponseResult;
import com.theus.ent.main.model.dto.bus.BusPatientTableAddDTO;
import com.theus.ent.main.model.dto.bus.BusPatientTableUpdateDTO;
import com.theus.ent.main.model.dto.bus.FindBusPatientTableDTO;
import com.theus.ent.main.model.po.bus.BusPatientTable;
import com.theus.ent.main.service.BusPatientTableService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author wangzhen
 * @date 2021/7/11 10:40
 * @Description:
 */
@Api(tags = "病例表单")
@RestController
@RequestMapping("/bus/patient/table")
public class BusPatientTableController {

    @Resource
    private BusPatientTableService busPatientTableService;

    @PostMapping(value = {"/add"})
    @ApiOperation(value = "添加病例表单")
    @SysLogs("添加病例表单")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<String> add(@RequestBody @Validated @ApiParam(value = "病例表单") BusPatientTableAddDTO addDTO) {

        busPatientTableService.add(addDTO);

        return ResponseResult.e(ResponseCode.OK);
    }

    @PostMapping(value = {"/update"})
    @ApiOperation(value = "更新病例表单")
    @SysLogs("更新病例表单")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<String> update(@RequestBody @Validated @ApiParam(value = "病例表单") BusPatientTableUpdateDTO updateDTO) {

        busPatientTableService.update(updateDTO.getTableCode(), updateDTO);

        return ResponseResult.e(ResponseCode.OK);
    }

    @PostMapping(value = {"/list"})
    @ApiOperation(value = "分页获取病例表单")
    @SysLogs("分页获取病例表单")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<IPage<BusPatientTable>> get(@RequestBody @Validated @ApiParam(value = "病例表单获取过滤条件") FindBusPatientTableDTO findBusPatientTableDTO) {

        return ResponseResult.e(ResponseCode.OK, busPatientTableService.findPage(findBusPatientTableDTO));
    }

    @PostMapping(value = {"/delete/{tableCode}"})
    @ApiOperation(value = "删除病例表单")
    @SysLogs("删除病例表单")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<String> delete(@PathVariable("tableCode") @ApiParam(value = "tableCode") String tableCode) {
        busPatientTableService.delete(tableCode);
        return ResponseResult.e(ResponseCode.OK);
    }
}

package com.theus.ent.main.controller;

import com.theus.ent.base.common.annotation.SysLogs;
import com.theus.ent.base.common.constants.DictConstants;
import com.theus.ent.core.bean.ResponseCode;
import com.theus.ent.core.bean.ResponseResult;
import com.theus.ent.main.model.dto.form.ConfigAddDTO;
import com.theus.ent.main.model.dto.form.ConfigUpdateDTO;
import com.theus.ent.main.model.dto.form.FormConfigDTO;
import com.theus.ent.main.service.FormConfigService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 表单配置管理控制器
 *
 * @author tangwei
 * @date 2021-07-04 10:51
 */
@RestController
@RequestMapping(value = "/form/config")
@Api(tags = "表单配置管理")
public class FormConfigController {

    @Resource
    private FormConfigService formConfigService;

    @GetMapping(value = "/list/{tableName}")
    @ApiOperation(value = "获取表单配置信息")
    @SysLogs("获取表单配置信息")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<List<FormConfigDTO>> getFormConfigList(@PathVariable("tableName") @ApiParam("表名") String tableName) {
        return ResponseResult.e(ResponseCode.OK, formConfigService.getFormConfigList(tableName));
    }

    @PostMapping(value = {"/hide"})
    @SysLogs("表单配置-隐藏列")
    @ApiOperation(value = "表单配置-隐藏列")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<String> hide(@RequestBody @Validated @ApiParam(value = "配置数据") ConfigUpdateDTO configUpdateDTO) {
        formConfigService.hide(configUpdateDTO);
        return ResponseResult.e(ResponseCode.OK);
    }

    @PostMapping(value = {"/show"})
    @SysLogs("表单配置-显示列")
    @ApiOperation(value = "表单配置-显示列")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<String> show(@RequestBody @Validated @ApiParam(value = "配置数据") ConfigUpdateDTO configUpdateDTO) {
        formConfigService.show(configUpdateDTO);
        return ResponseResult.e(ResponseCode.OK);
    }

    @PostMapping(value = {"/addExt"})
    @SysLogs("添加表单json配置")
    @ApiOperation(value = "添加表单json配置")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<String> addExt(@RequestBody @Validated @ApiParam(value = "配置数据") ConfigAddDTO configAddDTO) {
        formConfigService.addExt(configAddDTO);
        return ResponseResult.e(ResponseCode.OK);
    }

    @PostMapping(value = {"/updateExt"})
    @SysLogs("修改表单json配置")
    @ApiOperation(value = "修改表单json配置")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<String> updateExt(@RequestBody @Validated @ApiParam(value = "配置数据") ConfigUpdateDTO configUpdateDTO) {
        formConfigService.updateExt(configUpdateDTO);
        return ResponseResult.e(ResponseCode.OK);
    }

    @PostMapping(value = {"/deleteExt"})
    @SysLogs("删除表单json配置")
    @ApiOperation(value = "修改表单json配置")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<String> deleteExt(@RequestBody @Validated @ApiParam(value = "配置数据") ConfigUpdateDTO configUpdateDTO) {
        formConfigService.deleteExt(configUpdateDTO);
        return ResponseResult.e(ResponseCode.OK);
    }
}

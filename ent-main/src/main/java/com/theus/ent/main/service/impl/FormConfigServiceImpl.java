package com.theus.ent.main.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.theus.ent.base.common.constants.DictConstants;
import com.theus.ent.base.common.constants.SysConstants;
import com.theus.ent.base.model.dto.sqlserver.SysColumnsDTO;
import com.theus.ent.base.service.sqlserver.ColumnsService;
import com.theus.ent.core.exception.BusinessException;
import com.theus.ent.core.util.BaseConverter;
import com.theus.ent.main.mapper.FormConfigMapper;
import com.theus.ent.main.model.dto.form.ConfigAddDTO;
import com.theus.ent.main.model.dto.form.ConfigJsonDTO;
import com.theus.ent.main.model.dto.form.ConfigUpdateDTO;
import com.theus.ent.main.model.dto.form.FormConfigDTO;
import com.theus.ent.main.model.po.form.FormConfig;
import com.theus.ent.main.service.FormConfigService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author tangwei
 * @date 2021-07-04 10:53
 */
@Service
public class FormConfigServiceImpl extends ServiceImpl<FormConfigMapper, FormConfig> implements FormConfigService {

    @Resource
    private FormConfigMapper formConfigMapper;
    @Resource
    private ColumnsService columnsService;

    @Override
    public List<FormConfigDTO> getFormConfigList(String tableName) {
        // 表单配置信息
        List<FormConfig> formConfigList = formConfigMapper.getFormConfigs(tableName);
        List<FormConfigDTO> list = new ArrayList<>();
        // 元数据列信息
        List<SysColumnsDTO> sysColumnsList = columnsService.findList(tableName);
        if (CollectionUtils.isNotEmpty(sysColumnsList)) {
            // 按columnId倒序排序
            sysColumnsList = sysColumnsList.stream().sorted(Comparator.comparing(SysColumnsDTO::getColumnId).reversed())
                    .collect(Collectors.toList());
            // 循环表元数据列信息
            sysColumnsList.forEach(sysColumnsDTO -> {
                // 转为表单配置信息
                FormConfigDTO formConfigDTO = new BaseConverter<SysColumnsDTO, FormConfigDTO>().convert(sysColumnsDTO, FormConfigDTO.class);
                if (CollectionUtils.isNotEmpty(formConfigList)) {
                    // 匹配表单配置信息
                    List<FormConfig> tempList = formConfigList.stream().filter((formConfigPo -> formConfigPo.getColumnName().equals(sysColumnsDTO.getName())))
                            .collect(Collectors.toList());
                    if (CollectionUtils.isNotEmpty(tempList)) {
                        FormConfig formConfigPo = tempList.get(0);
                        BeanUtils.copyProperties(formConfigPo, formConfigDTO);
                    }
                }
                list.add(formConfigDTO);
            });
        }
        return list;
    }

    @Override
    public void hide(ConfigUpdateDTO configUpdateDTO) {
        FormConfig formConfig = formConfigMapper.getFormConfig(configUpdateDTO.getTableName(), configUpdateDTO.getColumnName());
        if (formConfig != null) {
            // 隐藏
            formConfig.setIsHide(DictConstants.YeaOrNot.YES.getValue());
            this.updateById(formConfig);
        } else {
            formConfig = new BaseConverter<ConfigUpdateDTO, FormConfig>().convert(configUpdateDTO, FormConfig.class);
            this.save(formConfig);
        }
    }

    @Override
    public void show(ConfigUpdateDTO configUpdateDTO) {
        FormConfig formConfig = formConfigMapper.getFormConfig(configUpdateDTO.getTableName(), configUpdateDTO.getColumnName());
        if (formConfig != null) {
            // 显示
            formConfig.setIsHide(DictConstants.YeaOrNot.NO.getValue());
            this.updateById(formConfig);
        } else {
            formConfig = new BaseConverter<ConfigUpdateDTO, FormConfig>().convert(configUpdateDTO, FormConfig.class);
            this.save(formConfig);
        }
    }

    @Override
    public void addExt(ConfigAddDTO configAddDTO) {
        FormConfig formConfig = formConfigMapper.getFormConfig(configAddDTO.getTableName(), configAddDTO.getColumnName());
        List<ConfigJsonDTO> list = new ArrayList<>();
        if (formConfig != null) {
            BeanUtils.copyProperties(configAddDTO, formConfig);
            formConfig.setIsDynamic(DictConstants.YeaOrNot.YES.getValue());
            ConfigJsonDTO configJsonDTO = configAddDTO.getJson();
            list = JSONObject.parseArray(formConfig.getJson(), ConfigJsonDTO.class);
            if (CollectionUtils.isNotEmpty(list)) {
                list.forEach(v -> {
                    // 如果字段id或者字段名称已存在 则提示不能重复
                    if (v.getField().equals(configJsonDTO.getField())) {
                        throw BusinessException.fail(String.format("字段ID %s 已存在，不能重复添加！", configJsonDTO.getField()));
                    }
                    if (v.getTitle().equals(configJsonDTO.getTitle())) {
                        throw BusinessException.fail(String.format("字段名称 %s 已存在，不能重复添加！", configJsonDTO.getTitle()));
                    }
                });
            }else{
                list = new ArrayList<>();
            }
            list.add(configJsonDTO);
            formConfig.setJson(JSONObject.toJSONString(list));
            this.updateById(formConfig);
        } else {
            formConfig = new BaseConverter<ConfigAddDTO, FormConfig>().convert(configAddDTO, FormConfig.class);
            formConfig.setIsDynamic(DictConstants.YeaOrNot.YES.getValue());
            list.add(configAddDTO.getJson());
            formConfig.setJson(JSONObject.toJSONString(list));
            this.save(formConfig);
        }
    }

    @Override
    public void updateExt(ConfigUpdateDTO configUpdateDTO) {
        FormConfig formConfig = formConfigMapper.selectById(configUpdateDTO.getId());
        if (formConfig != null) {
            BeanUtils.copyProperties(configUpdateDTO, formConfig);
            formConfig.setIsDynamic(DictConstants.YeaOrNot.YES.getValue());
            ConfigJsonDTO configJsonDTO = configUpdateDTO.getJson();
            List<ConfigJsonDTO> list = JSONObject.parseArray(formConfig.getJson(), ConfigJsonDTO.class);
            if (CollectionUtils.isNotEmpty(list)) {
                list.forEach(v -> {
                    // 字段id相同 则修改
                    if (v.getField().equals(configJsonDTO.getField())) {
                        BeanUtils.copyProperties(configJsonDTO, v);
                    }
                });
            }
            formConfig.setJson(JSONObject.toJSONString(list));
            formConfig.setIsDynamic(DictConstants.YeaOrNot.YES.getValue());
            this.updateById(formConfig);
        } else {
            throw BusinessException.fail(String.format("不存在表 %s 的扩展列配置数据，无法更新！", configUpdateDTO.getTableName()));
        }
    }

    @Override
    public void deleteExt(ConfigUpdateDTO configUpdateDTO) {
        FormConfig formConfig = formConfigMapper.selectById(configUpdateDTO.getId());
        if (formConfig != null) {
            BeanUtils.copyProperties(configUpdateDTO, formConfig);
            formConfig.setIsDynamic(DictConstants.YeaOrNot.YES.getValue());
            // 获取扩展列的配置项
            ConfigJsonDTO configJsonDTO = configUpdateDTO.getJson();
            List<ConfigJsonDTO> list = JSONObject.parseArray(formConfig.getJson(), ConfigJsonDTO.class);
            // 删除配置的该字段
            list.removeIf(item->item.getField().equals(configJsonDTO.getField()));
            formConfig.setJson(JSONObject.toJSONString(list));
            formConfig.setIsDynamic(DictConstants.YeaOrNot.YES.getValue());
            this.updateById(formConfig);
        } else {
            throw BusinessException.fail(String.format("不存在表 %s 的扩展列配置数据，无法删除！", configUpdateDTO.getTableName()));
        }
    }
}

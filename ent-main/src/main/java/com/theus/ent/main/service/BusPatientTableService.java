package com.theus.ent.main.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.theus.ent.main.model.dto.bus.BusPatientTableAddDTO;
import com.theus.ent.main.model.dto.bus.BusPatientTableUpdateDTO;
import com.theus.ent.main.model.dto.bus.FindBusPatientTableDTO;
import com.theus.ent.main.model.po.bus.BusPatientTable;

/**
 * @author wangzhen
 * @date 2021/7/9 21:15
 * @Description:
 */
public interface BusPatientTableService extends IService<BusPatientTable> {

    /**
     * 添加表单
     * @param addDTO
     */
    void add(BusPatientTableAddDTO addDTO);


    /**
     * 更新科添加表单
     * @param tableCode
     * @param updateDTO
     */
    void update(String tableCode, BusPatientTableUpdateDTO updateDTO);


    /**
     * 查询科添加表单（分页）
     * @param findBusPatientTableDTO 过滤条件
     * @return RequestResult
     */
    IPage<BusPatientTable> findPage(FindBusPatientTableDTO findBusPatientTableDTO);

    /**
     * 根据code删除表单
     * @param tableCode
     */
    void delete(String tableCode);
}

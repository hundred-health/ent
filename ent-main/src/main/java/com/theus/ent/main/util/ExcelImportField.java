package com.theus.ent.main.util;

import org.apache.poi.ss.usermodel.CellType;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Excel 导入配置注解
 *
 * @author zhaomj
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelImportField {
    /**
     * 导入字段在excel中的名字
     *
     * @return
     */
    @AliasFor("title")
    String value() default "";

    /**
     * 同 value
     *
     * @return
     */
    @AliasFor("value")
    String title() default "";

    /**
     * 是否必须字段，即是否不能为空值
     *
     * @return
     */
    boolean isRequired() default false;

    /**
     * 是否唯一
     * @return
     */
    boolean isUnique() default false;

    /**
     * 单元格数据类型枚举
     *
     * @return 单元格数据类型
     */
    CellType cellType() default CellType._NONE;
}

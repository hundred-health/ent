package com.theus.ent.main.controller;

import com.theus.ent.base.common.annotation.SysLogs;
import com.theus.ent.core.bean.ResponseCode;
import com.theus.ent.core.bean.ResponseResult;
import com.theus.ent.main.model.dto.bus.BusTopicTableAddDTO;
import com.theus.ent.main.model.dto.bus.DelBusTopicTableDTO;
import com.theus.ent.main.model.vo.bus.BusTopicTableVo;
import com.theus.ent.main.service.BusTopicTableService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author wangzhen
 * @date 2021/7/11 17:11
 * @Description:
 */
@Api(tags = "科研表单")
@RestController
@RequestMapping("/bus/topic/table")
public class BusTopicTableController {

    @Resource
    private BusTopicTableService busTopicTableService;

    @PostMapping(value = {"/add"})
    @ApiOperation(value = "添加科研病例表单")
    @SysLogs("添加科研病例表单")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<String> add(@RequestBody @Validated @ApiParam(value = "科研病例表单") List<BusTopicTableAddDTO> addDTOList) {

        busTopicTableService.addAll(addDTOList);

        return ResponseResult.e(ResponseCode.OK);
    }

    @PostMapping(value = {"/delete"})
    @ApiOperation(value = "删除科研病例表单")
    @SysLogs("删除科研病例表单")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<String> delete(@RequestBody @Validated @ApiParam(value = "删除科研病例表单") DelBusTopicTableDTO delBusTopicTableDTO) {

        busTopicTableService.deleteByCode(delBusTopicTableDTO);

        return ResponseResult.e(ResponseCode.OK);
    }

    @PostMapping(value = {"/get/{topicCode}"})
    @ApiOperation(value = "根据科研code获取科研病例表单")
    @SysLogs("根据科研code获取科研病例表单")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<List<BusTopicTableVo>> getUser(@PathVariable("topicCode") @ApiParam(value = "topicCode") String topicCode) {
        return ResponseResult.e(ResponseCode.OK, busTopicTableService.findByBusTopicCode(topicCode));
    }
}

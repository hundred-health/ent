package com.theus.ent.main.model.po.bus;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author wangzhen
 * @date 2021/7/9 20:20
 * @Description: 科研主题与患者关系
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BusTopicPatient implements Serializable {

    @TableId
    private String code;

    private String topicCode;

    private String patientCode;
}

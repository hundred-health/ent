package com.theus.ent.main.model.dto.bus;

import com.theus.ent.base.model.dto.BaseSplitPageDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper=true)
public class FindBusTopicsDTO extends BaseSplitPageDTO {

    /**
     * 科研名称
     */
    private String topicName;


}

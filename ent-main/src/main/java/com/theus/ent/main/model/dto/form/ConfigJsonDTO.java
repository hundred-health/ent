package com.theus.ent.main.model.dto.form;

import lombok.Data;

/**
 * @author tangwei
 * @date 2021-07-10 20:48
 */
@Data
public class ConfigJsonDTO {

    /**
     * 字段id
     */
    private String field;

    /**
     * 字段名称
     */
    private String title;

    /**
     * 组件类型
     */
    private String type;

    /**
     * 字典分类
     */
    private String dictClass;

    /**
     * 隐藏
     */
    private String isHide;

    /**
     * 占位提示
     */
    private String placeholder;

    /**
     * 是否必填
     */
    private String required;
}

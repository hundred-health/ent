package com.theus.ent.main.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.FatalBeanException;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;
import org.springframework.util.StringUtils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author fangyou
 * @date 2019/7/31 18:04
 */
public class EntBeanUtils extends BeanUtils {

    private static final String LOG_MSG = "其他条件，不做处理";
    private static final String ERRMSG = "Could not copy property'%s'from source to target";
    private static final Logger LOGGER = LoggerFactory.getLogger(EntBeanUtils.class);

    public static void copyProperties(Object source, Object target) throws BeansException {
        copyPropBigDecimal2String(source, target, (Class) null, (String[]) null);
    }

    public static void copyPropString2BigDecimal(Object source, Object target) throws BeansException {
        copyPropString2BigDecimal(source, target, (Class) null, (String[]) null);
    }

    private static void copyPropBigDecimal2String(Object source, Object target, Class<?> editable, String... ignoreProperties) throws BeansException {
        Assert.notNull(source, "Source must not be null");
        Assert.notNull(target, "Target must not be null");
        Class<?> actualEditable = target.getClass();
        if (editable != null) {
            if (!editable.isInstance(target)) {
                throw new IllegalArgumentException("Target class [" + target.getClass().getName() + "] not assignable to Editable class [" + editable.getName() + "]");
            }
            actualEditable = editable;
        }
        PropertyDescriptor[] targetPds = getPropertyDescriptors(actualEditable);
        List<String> ignoreList = ignoreProperties != null ? Arrays.asList(ignoreProperties) : new ArrayList<>();
        PropertyDescriptor[] var7 = targetPds;
        int var8 = targetPds.length;
        for (int var9 = 0; var9 < var8; ++var9) {
            PropertyDescriptor targetPd = var7[var9];
            try {
                Method writeMethod = targetPd.getWriteMethod();
                PropertyDescriptor sourcePd = getPropertyDescriptor(source.getClass(), targetPd.getName());
                if (writeMethod == null || ignoreList.contains(targetPd.getName()) || sourcePd == null) {
                    continue;
                }
                Method readMethod = sourcePd.getReadMethod();
                if (readMethod != null && ClassUtils.isAssignable(writeMethod.getParameterTypes()[0], readMethod.getReturnType())) {
                    if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
                        readMethod.setAccessible(true);
                    }
                    if (!Modifier.isPublic(writeMethod.getDeclaringClass().getModifiers())) {
                        writeMethod.setAccessible(true);
                    }
                    writeMethod.invoke(target, readMethod.invoke(source));
                } else if (readMethod != null && null != writeMethod) {
                    boolean b1 = String.class.isAssignableFrom(writeMethod.getParameterTypes()[0]);
                    boolean b2 = BigDecimal.class.isAssignableFrom(readMethod.getReturnType());
                    boolean b3 = writeMethod.getName().substring(3).equals(readMethod.getName().substring(3));
                    if (b1 && b2 && b3) {
                        if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
                            readMethod.setAccessible(true);
                        }
                        if (!Modifier.isPublic(writeMethod.getDeclaringClass().getModifiers())) {
                            writeMethod.setAccessible(true);
                        }
                        Object value = readMethod.invoke(source);
                        writeMethod.invoke(target, value==null ||"".equals(value)?null:new BigDecimal(value.toString()).toPlainString());
                    }
                } else {
                    LOGGER.info(LOG_MSG);
                }
            } catch (Exception e) {
                throw new FatalBeanException(String.format(ERRMSG, targetPd.getName()), e);
            }
        }
    }

    private static void copyPropString2BigDecimal(Object source, Object target, Class<?> editable, String... ignoreProperties) throws BeansException {
        Assert.notNull(source, "Source must not be null");
        Assert.notNull(target, "Target must not be null");
        Class<?> actualEditable = target.getClass();
        if (editable != null) {
            if (!editable.isInstance(target)) {
                throw new IllegalArgumentException("Target class [" + target.getClass().getName() + "] not assignable to Editable class [" + editable.getName() + "]");
            }
            actualEditable = editable;
        }
        PropertyDescriptor[] targetPds = getPropertyDescriptors(actualEditable);
        List<String> ignoreList = ignoreProperties != null ? Arrays.asList(ignoreProperties) : new ArrayList<>();
        PropertyDescriptor[] var7 = targetPds;
        int var8 = targetPds.length;
        for (int var9 = 0; var9 < var8; ++var9) {
            PropertyDescriptor targetPd = var7[var9];
            try {
                Method writeMethod = targetPd.getWriteMethod();
                PropertyDescriptor sourcePd = getPropertyDescriptor(source.getClass(), targetPd.getName());
                if (writeMethod == null || ignoreList.contains(targetPd.getName()) || sourcePd == null) {
                    continue;
                }
                Method readMethod = sourcePd.getReadMethod();
                if (readMethod != null && ClassUtils.isAssignable(writeMethod.getParameterTypes()[0], readMethod.getReturnType())) {
                    if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
                        readMethod.setAccessible(true);
                    }
                    Object value = readMethod.invoke(source);
                    if (!Modifier.isPublic(writeMethod.getDeclaringClass().getModifiers())) {
                        writeMethod.setAccessible(true);
                    }
                    writeMethod.invoke(target, value);
                } else if (readMethod != null && null != writeMethod) {
                    boolean b1 = BigDecimal.class.isAssignableFrom(writeMethod.getParameterTypes()[0]);
                    boolean b2 = String.class.isAssignableFrom(readMethod.getReturnType());
                    boolean b3 = writeMethod.getName().substring(3).equals(readMethod.getName().substring(3));
                    if (b1 && b2 && b3) {
                        if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
                            readMethod.setAccessible(true);
                        }
                        Object value = readMethod.invoke(source);
                        if (!Modifier.isPublic(writeMethod.getDeclaringClass().getModifiers())) {
                            writeMethod.setAccessible(true);
                        }
                        if (null != value && !StringUtils.isEmpty(value.toString())) {
                            writeMethod.invoke(target, new BigDecimal(value.toString()));
                        } else {
                            writeMethod.invoke(target, new Object[]{null});
                        }
                    }
                } else {
                    LOGGER.info(LOG_MSG);
                }
            } catch (Exception e) {
                throw new FatalBeanException(String.format(ERRMSG, targetPd.getName()), e);
            }
        }
    }

    public static void copyOldPropString2BigDecimal(Object source, Object target) throws BeansException {
        Assert.notNull(source, "Source must not be null");
        Assert.notNull(target, "Target must not be null");
        Class<?> editable = null;
        String[] ignoreProperties = null;
        Class<?> actualEditable = target.getClass();
        if (editable != null) {
            if (!editable.isInstance(target)) {
                throw new IllegalArgumentException("Target class [" + target.getClass().getName() + "] not assignable to Editable class [" + editable.getName() + "]");
            }
            actualEditable = editable;
        }
        PropertyDescriptor[] targetPds = getPropertyDescriptors(actualEditable);
        List<String> ignoreList = ignoreProperties != null ? Arrays.asList(ignoreProperties) : new ArrayList<>();
        PropertyDescriptor[] var7 = targetPds;
        int var8 = targetPds.length;
        for (int var9 = 0; var9 < var8; ++var9) {
            PropertyDescriptor targetPd = var7[var9];
            try {
                Method writeMethod = targetPd.getWriteMethod();
                PropertyDescriptor sourcePd = getPropertyDescriptor(source.getClass(), targetPd.getName());
                if (writeMethod == null || !targetPd.getName().endsWith("Old") || ignoreList.contains(targetPd.getName()) || sourcePd == null) {
                    continue;
                }
                Method readMethod = sourcePd.getReadMethod();
                if (readMethod != null && ClassUtils.isAssignable(writeMethod.getParameterTypes()[0], readMethod.getReturnType())) {
                    if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
                        readMethod.setAccessible(true);
                    }
                    Object value = readMethod.invoke(source);
                    if (!Modifier.isPublic(writeMethod.getDeclaringClass().getModifiers())) {
                        writeMethod.setAccessible(true);
                    }
                    writeMethod.invoke(target, value);
                } else if (readMethod != null && null != writeMethod) {
                    boolean b1 = BigDecimal.class.isAssignableFrom(writeMethod.getParameterTypes()[0]);
                    boolean b2 = String.class.isAssignableFrom(readMethod.getReturnType());
                    boolean b3 = writeMethod.getName().substring(3).equals(readMethod.getName().substring(3));
                    if (b1 && b2 && b3) {
                        if (!Modifier.isPublic(readMethod.getDeclaringClass().getModifiers())) {
                            readMethod.setAccessible(true);
                        }
                        Object value = readMethod.invoke(source);
                        if (!Modifier.isPublic(writeMethod.getDeclaringClass().getModifiers())) {
                            writeMethod.setAccessible(true);
                        }
                        if (null != value && !StringUtils.isEmpty(value.toString())) {
                            writeMethod.invoke(target, new BigDecimal(value.toString()));
                        } else {
                            writeMethod.invoke(target, new Object[]{null});
                        }
                    }
                } else {
                    LOGGER.info("其他条件，不做处理");
                }
            } catch (Exception e) {
                throw new FatalBeanException(String.format(ERRMSG, targetPd.getName()), e);
            }
        }
    }
}



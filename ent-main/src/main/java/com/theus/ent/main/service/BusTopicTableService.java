package com.theus.ent.main.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.theus.ent.main.model.dto.bus.BusTopicTableAddDTO;
import com.theus.ent.main.model.dto.bus.DelBusTopicTableDTO;
import com.theus.ent.main.model.po.bus.BusTopicTable;
import com.theus.ent.main.model.vo.bus.BusTopicTableVo;

import java.util.List;

/**
 * @author wangzhen
 * @date 2021/7/9 21:16
 * @Description:
 */
public interface BusTopicTableService extends IService<BusTopicTable> {

    /**
     * 添加科研主题表单
     * @param addDTO
     */
    void add(BusTopicTableAddDTO addDTO);

    /**
     * 查询科研主题关联的表单
     * @param topicCode
     * @return
     */
    List<BusTopicTableVo> findByBusTopicCode(String topicCode);

    /**
     * 根据主键删除关联关系
     * @param code
     */
    void deleteByCode(DelBusTopicTableDTO delBusTopicTableDTO);

    /**
     * 批量添加
     * @param addDTOList
     */
    void addAll(List<BusTopicTableAddDTO> addDTOList);
}

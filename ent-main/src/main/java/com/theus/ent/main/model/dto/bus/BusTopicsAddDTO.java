package com.theus.ent.main.model.dto.bus;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author wangzhen
 * @date 2021/7/9 22:27
 * @Description:
 */
@Data
public class BusTopicsAddDTO {

    @NotBlank(message = "科研主题名称不可以为空！")
    private String topicName;

    @NotBlank(message = "责任医生编码不可以为空！")
    private String docCode;

    @NotBlank(message = "科研主题状态不可以为空！")
    private String topicState;

}

package com.theus.ent.main.util;

import cn.hutool.core.util.StrUtil;

/**
 * @author tangwei
 * @date 2020-09-11 16:47
 */
public class PathUtil {

    /**
     * windows路径转为网页路径
     * @param path windows路径
     * @return url路径
     */
    public static String windowsPathToUrl(String path) {
        if (StrUtil.isNotBlank(path)) {
            return path.replace('\\', '/');
        } else {
            return path;
        }
    }
}

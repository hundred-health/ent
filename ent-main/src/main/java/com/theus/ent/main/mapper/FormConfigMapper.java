package com.theus.ent.main.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.theus.ent.main.model.po.form.FormConfig;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author tangwei
 * @date 2021-07-04 18:55
 */
@Mapper
@Repository
public interface FormConfigMapper extends BaseMapper<FormConfig> {

    /**
     * 获取表单配置列表
     *
     * @param tableName 表名
     * @return 配置列表
     */
    List<FormConfig> getFormConfigs(String tableName);

    /**
     * 获取表单配置信息
     *
     * @param tableName 表名
     * @param columnName 列名
     * @return 表单配置信息
     */
    FormConfig getFormConfig(String tableName, String columnName);
}

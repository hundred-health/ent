package com.theus.ent.main.util;

import java.util.UUID;

/**
 * @author wangzhen
 * @date 2021/7/10 9:53
 * @Description:
 */
public class UUIDUtil {

    public static String getUUID() {

        return UUID.randomUUID().toString();
    }
}

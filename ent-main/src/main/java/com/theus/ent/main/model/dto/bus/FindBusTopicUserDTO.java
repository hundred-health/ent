package com.theus.ent.main.model.dto.bus;

import com.theus.ent.base.model.dto.BaseSplitPageDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;


@Data

public class FindBusTopicUserDTO  {

    /**
     * 科研code
     */
    private String topicCode;

    /**
     * 医生姓名
     */
    private String doctorName;
}

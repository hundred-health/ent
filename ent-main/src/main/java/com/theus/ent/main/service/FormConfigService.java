package com.theus.ent.main.service;

import com.theus.ent.main.model.dto.form.ConfigAddDTO;
import com.theus.ent.main.model.dto.form.ConfigUpdateDTO;
import com.theus.ent.main.model.dto.form.FormConfigDTO;

import java.util.List;

/**
 * @author tangwei
 * @date 2021-07-04 10:53
 */
public interface FormConfigService {

    /**
     * 获取表单配置信息
     *
     * @param tableName 表名
     * @return 表单配置信息
     */
    List<FormConfigDTO> getFormConfigList(String tableName);

    /**
     * 隐藏列
     * @param configUpdateDTO 配置信息
     */
    void hide(ConfigUpdateDTO configUpdateDTO);

    /**
     * 显示列
     * @param configUpdateDTO 配置信息
     */
    void show(ConfigUpdateDTO configUpdateDTO);

    /**
     * 添加配置列
     *
     * @param configAddDTO 配置信息
     */
    void addExt(ConfigAddDTO configAddDTO);

    /**
     * 修改配置列
     *
     * @param configUpdateDTO 配置信息
     */
    void updateExt(ConfigUpdateDTO configUpdateDTO);

    /**
     * 删除配置列
     *
     * @param configUpdateDTO 配置信息
     */
    void deleteExt(ConfigUpdateDTO configUpdateDTO);
}

package com.theus.ent.main.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.theus.ent.base.model.po.system.SysUser;
import com.theus.ent.main.model.dto.bus.BusTopicUserAddDTO;
import com.theus.ent.main.model.dto.bus.DelBusTopicUserDTO;
import com.theus.ent.main.model.dto.bus.FindBusTopicUserDTO;
import com.theus.ent.main.model.po.bus.BusTopicUser;
import com.theus.ent.main.model.vo.bus.BusTopicUserVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author wangzhen
 * @date 2021/7/9 20:42
 * @Description:
 */
public interface BusTopicUserService extends IService<BusTopicUser> {
    /**
     * 添加科研主题医生
     * @param addDTO
     */
    void add(BusTopicUserAddDTO addDTO);

    /**
     * 查询科研主题关联的医生
     * @param findBusTopicUserDTO
     * @return
     */
    List<BusTopicUserVo> findByBusTopic(FindBusTopicUserDTO findBusTopicUserDTO);

    /**
     * 统计科研主题医生
     * @param topicCode
     * @return
     */
    int countByTopicCode(String topicCode);

    /**
     * 删除科研主题医生
     * @param code
     */
    void deleteByCode(DelBusTopicUserDTO code);

    String getItemName( String name, String itemValue);

    void addAll(List<BusTopicUserAddDTO> busTopicUserAddDTOs);
}

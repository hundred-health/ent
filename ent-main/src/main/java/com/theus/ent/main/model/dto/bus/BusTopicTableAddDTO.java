package com.theus.ent.main.model.dto.bus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * @author wangzhen
 * @date 2021/7/11 16:03
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BusTopicTableAddDTO {

    @NotBlank(message = "科研主题名称不可以为空！")
    private String topicCode; //科研主题编号

    @NotBlank(message = "表单编号不可以为空！")
    private String tableCode; //	表单编号
}

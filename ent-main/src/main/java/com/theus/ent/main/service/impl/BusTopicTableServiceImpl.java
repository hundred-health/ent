package com.theus.ent.main.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.theus.ent.base.common.constants.DictConstants;
import com.theus.ent.core.exception.BusinessException;
import com.theus.ent.main.mapper.BusTopicTableMapper;
import com.theus.ent.main.model.dto.bus.BusTopicTableAddDTO;
import com.theus.ent.main.model.dto.bus.DelBusTopicTableDTO;
import com.theus.ent.main.model.po.bus.BusPatientTable;
import com.theus.ent.main.model.po.bus.BusTopicTable;
import com.theus.ent.main.model.vo.bus.BusTopicTableVo;
import com.theus.ent.main.service.BusPatientTableService;
import com.theus.ent.main.service.BusTopicTableService;
import com.theus.ent.main.util.UUIDUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author wangzhen
 * @date 2021/7/11 16:20
 * @Description:
 */
@Service
public class BusTopicTableServiceImpl extends ServiceImpl<BusTopicTableMapper, BusTopicTable> implements BusTopicTableService {

    @Autowired
    private BusTopicTableMapper busTopicTableMapper;

    @Autowired
    private BusPatientTableService busPatientTableService;

    @Override
    public void add(BusTopicTableAddDTO addDTO) {


    }

    @Override
    public List<BusTopicTableVo> findByBusTopicCode(String topicCode) {

        QueryWrapper<BusTopicTable> busTopicTableQueryWrapper = new QueryWrapper<>();

        busTopicTableQueryWrapper.eq("topic_code",topicCode);

        List<BusTopicTable> busTopicTableList = busTopicTableMapper.selectList(busTopicTableQueryWrapper);

        List<BusTopicTableVo> busTopicTableVoList = busTopicTableList.stream().map(busTopicTable -> {

            BusPatientTable busPatientTable = busPatientTableService.getById(busTopicTable.getTableCode());

            BusTopicTableVo busTopicTableVo = new BusTopicTableVo();

            busTopicTableVo.setCode(busTopicTable.getCode());

            busTopicTableVo.setTopicCode(topicCode);

            busTopicTableVo.setTableCode(busPatientTable.getTableCode());

            busTopicTableVo.setTableName(busPatientTable.getTableName());

            busTopicTableVo.setTableCartegory(busPatientTable.getTableCartegory());

            return busTopicTableVo;
        }).collect(Collectors.toList());

        return busTopicTableVoList;
    }

    @Override
    @Transactional
    public void deleteByCode(DelBusTopicTableDTO delBusTopicTableDTO) {

        for(String code : delBusTopicTableDTO.getCodes()) {

            BusTopicTable busTopicTable = this.getById(code);

            if (busTopicTable == null) {
                continue;
            }

            try {
                this.removeById(code);

            } catch (BusinessException e) {
                throw BusinessException.fail(e.getMsg(), e);
            } catch (Exception e) {
                throw BusinessException.fail("科研主题关联表单删除失败", e);
            }
        }

    }

    @Override
    public void addAll(List<BusTopicTableAddDTO> addDTOList) {

        for(BusTopicTableAddDTO addDTO : addDTOList) {

            QueryWrapper<BusTopicTable> busTopicPatientQueryWrapper = new QueryWrapper<>();

            busTopicPatientQueryWrapper.eq("topic_code",addDTO.getTopicCode());

            busTopicPatientQueryWrapper.eq("table_code",addDTO.getTableCode());

            Integer selectCount = busTopicTableMapper.selectCount(busTopicPatientQueryWrapper);

            if(selectCount >=1) {
              continue;
            }

            BusTopicTable busTopicTable = new BusTopicTable();

            BeanUtils.copyProperties(addDTO,busTopicTable);

            busTopicTable.setCode(UUIDUtil.getUUID());

            this.save(busTopicTable);
        }
    }
}

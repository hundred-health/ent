package com.theus.ent.main.model.po.bus;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author wangzhen
 * @date 2021/7/9 21:00
 * @Description: 病例表单
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BusPatientTable implements Serializable {

    @TableId
    private String tableCode; //表单编号

    private String tableName; //表单名称

    private String tableUrl; //表单路径

    /**
     * 首次诊断
     * 入组前访视
     * 入排标准
     * 手术组
     * 放疗组
     * 随访
     */
    private String tableCartegory; //表单分类

    // 是否删除
    private Integer delFlag;
}

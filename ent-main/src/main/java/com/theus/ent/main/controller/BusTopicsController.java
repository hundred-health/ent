package com.theus.ent.main.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.theus.ent.base.common.annotation.SysLogs;
import com.theus.ent.core.bean.ResponseCode;
import com.theus.ent.core.bean.ResponseResult;
import com.theus.ent.main.model.dto.bus.*;
import com.theus.ent.main.model.po.bus.BusTopics;
import com.theus.ent.main.service.BusTopicsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;


/**
 * @author wangzhen
 * @date 2021/7/9 21:43
 * @Description:
 */
@Api(tags = "科研主题")
@RestController
@RequestMapping("/bus/topics")
public class BusTopicsController {

    @Resource
    private BusTopicsService busTopicsService;

    @PostMapping(value = {"/add"})
    @ApiOperation(value = "添加科研主题")
    @SysLogs("添加科研主题")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<String> add(@RequestBody @Validated @ApiParam(value = "科研主题") BusTopicsAddDTO addDTO) {
        busTopicsService.add(addDTO);
        return ResponseResult.e(ResponseCode.OK);
    }
    @PostMapping(value = {"/update"})
    @ApiOperation(value = "更新科研主题")
    @SysLogs("更新科研主题")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<String> updateNew(@RequestBody @Validated @ApiParam(value = "科研主题") BusTopicsUpdateDTO updateDTO) {
        busTopicsService.update(updateDTO.getTopicCode(), updateDTO);
        return ResponseResult.e(ResponseCode.OK);
    }

    @PostMapping(value = {"/list"})
    @ApiOperation(value = "分页获取科研主题数据")
    @SysLogs("分页获取科研主题数据")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<IPage<BusTopics>> get(@RequestBody @Validated @ApiParam(value = "科研主题取过滤条件") FindBusTopicsDTO findBusTopicsDTO) {

        return ResponseResult.e(ResponseCode.OK, busTopicsService.findPage(findBusTopicsDTO));
    }

    @PostMapping(value = {"/delete/{topicCode}"})
    @ApiOperation(value = "删除科研主题")
    @SysLogs("删除科研主题")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<String> delete(@PathVariable("topicCode") @ApiParam(value = "topicCode") String topicCode) {
        busTopicsService.delete(topicCode);
        return ResponseResult.e(ResponseCode.OK);
    }


}

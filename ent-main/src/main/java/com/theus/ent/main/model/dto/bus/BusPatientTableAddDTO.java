package com.theus.ent.main.model.dto.bus;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author wangzhen
 * @date 2021/7/11 10:42
 * @Description:
 */
@Data
public class BusPatientTableAddDTO {

    @NotBlank(message = "表单名称不可以为空！")
    private String tableName; //表单名称

    @NotBlank(message = "表单路径不可以为空！")
    private String tableUrl; //表单路径

    /**
     * 首次诊断
     * 入组前访视
     * 入排标准
     * 手术组
     * 放疗组
     * 随访
     */
    @NotBlank(message = "表单分类不可以为空！")
    private String tableCartegory; //表单分类
}

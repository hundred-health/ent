package com.theus.ent.main.controller;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.theus.ent.base.common.annotation.SysLogs;
import com.theus.ent.core.bean.ResponseCode;
import com.theus.ent.core.bean.ResponseResult;
import com.theus.ent.main.model.dto.bus.BusPatientDTO;
import com.theus.ent.main.model.dto.bus.PageQueryDTO;
import com.theus.ent.main.model.vo.bus.BusPatientVO;
import com.theus.ent.main.service.BusPatientService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author fangkun
 * @since 2021-07-12
 */
@Api(tags = "患者信息")
@RestController
@RequestMapping("/bus-patient")
public class BusPatientController {
    @Resource
    private BusPatientService busPatientService;

    @PostMapping("/save")
    @SysLogs("保存患者信息")
    @ApiOperation(value = "保存患者信息")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult save(@RequestBody @Validated @ApiParam(value = "查询条件")
                                       BusPatientDTO dto) {
        busPatientService.savePatient(dto);
        return ResponseResult.e(ResponseCode.OK);
    }

    @GetMapping("/load")
    @SysLogs("根据主键查询患者信息")
    @ApiOperation(value = "根据主键查询患者信息")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<BusPatientVO> getProblemMonthNum(@ApiParam(value = "患者编号") String patientCode) {
        return ResponseResult.e(ResponseCode.OK,busPatientService.load(patientCode));
    }

    @DeleteMapping("/delete")
    @SysLogs("根据主键逻辑删除患者信息")
    @ApiOperation(value = "根据主键逻辑删除患者信息")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult delete(@ApiParam(value = "患者编号") String patientCode) {
        busPatientService.delete(patientCode);
        return ResponseResult.e(ResponseCode.OK);
    }

    @PostMapping("/page-query")
    @SysLogs("分页查询患者信息")
    @ApiOperation(value = "分页查询患者信息")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<IPage<BusPatientVO>> pageQuery(@RequestBody @Validated @ApiParam(value = "查询条件") PageQueryDTO dto) {
        return ResponseResult.e(ResponseCode.OK,busPatientService.pageQuery(dto));
    }

    @PostMapping("/import-template")
    @ApiOperation(value = "模板导入")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult pageQuery(@RequestBody @ApiParam(value = "导入文件") MultipartFile file) {
        busPatientService.importTemplate(file);
        return ResponseResult.e(ResponseCode.OK);
    }
}

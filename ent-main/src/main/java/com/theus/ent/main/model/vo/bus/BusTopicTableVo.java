package com.theus.ent.main.model.vo.bus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wangzhen
 * @date 2021/7/11 16:17
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BusTopicTableVo {

    private String code;

    private String tableCode;//

    private String topicCode; //科研code

    private String tableName; //表单名称

    private String tableCartegory; //表单分类
}

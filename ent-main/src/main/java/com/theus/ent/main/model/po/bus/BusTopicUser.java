package com.theus.ent.main.model.po.bus;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author wangzhen
 * @date 2021/7/9 20:19
 * @Description: 科研主题与医生关系
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BusTopicUser implements Serializable {

    @TableId
    private String code; //关系编号

    private String topicCode; //科研主题编号

    private String doctorCode; //	医生编号
}

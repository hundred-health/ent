package com.theus.ent.main.model.dto.bus;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author wangzhen
 * @date 2021/7/10 16:42
 * @Description:
 */
@Data
public class BusTopicUserAddDTO {

    @NotBlank(message = "科研主题名称不可以为空！")
    private String topicCode; //科研主题编号

    @NotBlank(message = "医生编号不可以为空！")
    private String doctorCode; //	医生编号
}

package com.theus.ent.main.model.vo;

import lombok.Data;

/**
 * @author tangwei
 * @date 2020-09-17 21:09
 */
@Data
public class UserVO {

    /**
     * 用户账号
     */
    private String account;

    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 所属机构名称
     */
    private String organName;

    /**
     * 手机号
     */
    private String telephone;
}

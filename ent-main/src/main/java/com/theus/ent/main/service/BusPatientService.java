package com.theus.ent.main.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.theus.ent.main.model.dto.bus.BusPatientDTO;
import com.theus.ent.main.model.dto.bus.PageQueryDTO;
import com.theus.ent.main.model.po.bus.BusPatient;
import com.theus.ent.main.model.vo.bus.BusPatientVO;
import org.springframework.web.multipart.MultipartFile;


/**
 * <p>
 * 服务类
 * </p>
 *
 * @author fangkun
 * @since 2021-07-12
 */
public interface BusPatientService extends IService<BusPatient> {
    /**
     * 保存患者信息
     *
     * @param dto 患者信息
     */
    void savePatient(BusPatientDTO dto);

    /**
     * 根据患者号查询患者信息
     *
     * @param patientCode 患者号
     * @return 患者信息
     */
    BusPatientVO load(String patientCode);

    /**
     * 删除患者信息
     *
     * @param patientCode
     */
    void delete(String patientCode);

    /**
     * 分页查询患者信息
     *
     * @param dto 分页查询参数
     * @return 分页数据
     */
    IPage<BusPatientVO> pageQuery(PageQueryDTO dto);

    /**
     * 模板导入
     *
     * @param file 导入文件
     */
    void importTemplate(MultipartFile file);
}

package com.theus.ent.main.model.vo.bus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wangzhen
 * @date 2021/7/10 21:34
 * @Description:
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class BusTopicUserVo {

    private String code;
    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 手机号码
     */
    private String telephone;

    /**
     * 科室
     */
    private String deptName;

    /**
     * 头衔
     */
    private String title;
}

package com.theus.ent.main.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.theus.ent.main.model.po.bus.BusTopicTable;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;


@Mapper
@Repository
public interface BusTopicTableMapper extends BaseMapper<BusTopicTable> {


}

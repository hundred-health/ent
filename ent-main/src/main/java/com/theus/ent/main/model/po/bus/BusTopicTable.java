package com.theus.ent.main.model.po.bus;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author wangzhen
 * @date 2021/7/9 21:02
 * @Description: 科研主题与表单关系
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BusTopicTable implements Serializable {

    @TableId
    private String code;

    private String topicCode;

    private String tableCode;
}

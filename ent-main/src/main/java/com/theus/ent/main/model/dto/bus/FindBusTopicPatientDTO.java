package com.theus.ent.main.model.dto.bus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * @author wangzhen
 * @date 2021/7/16 9:05
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FindBusTopicPatientDTO {

    @NotBlank(message = "科研code不可以为空！")
    private String topicCode;

    private String patientName;
}

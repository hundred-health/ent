package com.theus.ent.main.model.dto.bus;

import lombok.Data;

import java.util.List;

/**
 * @author wz
 * @date 2021/7/16 11:26
 * @description
 */
@Data
public class DelBusTopicTableDTO {

    private List<String> codes;
}

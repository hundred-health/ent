package com.theus.ent.main.controller;

import com.theus.ent.base.common.annotation.SysLogs;
import com.theus.ent.core.bean.ResponseCode;
import com.theus.ent.core.bean.ResponseResult;
import com.theus.ent.main.model.dto.bus.BusTopicPatientAddDTO;
import com.theus.ent.main.model.dto.bus.DelBusTopicPatientDTO;
import com.theus.ent.main.model.dto.bus.FindBusTopicPatientDTO;
import com.theus.ent.main.model.vo.bus.BusTopicPatientVo;
import com.theus.ent.main.model.vo.bus.BusTopicUserVo;
import com.theus.ent.main.service.BusTopicPatientService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author wangzhen
 * @date 2021/7/10 15:55
 * @Description:
 */
@Api(tags = "添加科研患者")
@RestController
@RequestMapping("/bus/topic/patient")
public class BusTopicPatientController {

    @Resource
    private BusTopicPatientService busPatientService;

    @PostMapping(value = {"/patient/add"})
    @ApiOperation(value = "添加科研患者")
    @SysLogs("添加科研患者")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<String> add(@RequestBody @Validated @ApiParam(value = "添加科研患者") List<BusTopicPatientAddDTO> addDTOs) {
        busPatientService.addAll(addDTOs);
        return ResponseResult.e(ResponseCode.OK);
    }
    @PostMapping(value = {"/delete"})
    @ApiOperation(value = "删除科研患者")
    @SysLogs("删除科研患者")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<String> delete(@RequestBody @Validated @ApiParam(value = "删除科研患者") DelBusTopicPatientDTO delBusTopicPatientDTO) {

        busPatientService.deleteByCode(delBusTopicPatientDTO);

        return ResponseResult.e(ResponseCode.OK);
    }

    @PostMapping(value = {"/get"})
    @ApiOperation(value = "根据科研code获取科研患者")
    @SysLogs("根据科研code获取科研患者")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<List<BusTopicPatientVo>> getUser(@RequestBody @Validated @ApiParam(value = "根据科研code获取科研患者")FindBusTopicPatientDTO findBusTopicPatientDTO) {
        return ResponseResult.e(ResponseCode.OK, busPatientService.findByBusTopicCode(findBusTopicPatientDTO));
    }
}

package com.theus.ent.main.model.vo.bus;

import lombok.Data;

import java.time.LocalDate;
@Data
public class BusPatientVO {
    /**
     * 患者号
     */
    private String patientCode;
    /**
     * 实验编号
     */
    private String testCode;
    /**
     * 患者姓名
     */
    private String patientName;
    /**
     * 手机号码
     */
    private String phone;
    /**
     * 身份证号
     */
    private String cardNo;
    /**
     * 出生日期
     */
    private LocalDate birthday;
    /**
     * 性别
     */
    private String gender;
    /**
     * 住址
     */
    private String address;
    /**
     * 身高
     */
    private String height;
    /**
     * 体重
     */
    private String weight;
    /**
     * 门诊号
     */
    private String outpatientNo;
    /**
     * 住院号
     */
    private String inpatientNO;
    /**
     * 签署知情同意书
     */
    private String signState;
    /**
     * 创建人id
     */
    private String createUserId;
    /**
     * 创建人姓名
     */
    private String createUserName;
}

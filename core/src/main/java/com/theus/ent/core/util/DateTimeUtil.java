package com.theus.ent.core.util;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;

import java.util.Calendar;
import java.util.Date;

/**
 * 日期工具类
 * @author tangwei
 * @date 2020-10-30 21:10
 */
public class DateTimeUtil {

    /**
     * 获取当前时间的yyyyMMddHHmmssSSS格式字符串 17位
     * @return 2020103021155266
     */
    public static String toMsStr() {
        return new DateTime().toString(DatePattern.PURE_DATETIME_MS_PATTERN);
    }

    /**
     * 获取今天 零点
     *
     * @return 日期
     */
    public static Date todayZero(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime();
    }
}

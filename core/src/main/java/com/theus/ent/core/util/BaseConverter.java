package com.theus.ent.core.util;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tangwei
 * @date 2018-07-12 15:18
 * Specification : 文档说明
 */
public class BaseConverter<SOURCE, TARGET> {
    private static final Logger logger = LoggerFactory.getLogger(BaseConverter.class);

    /**
     * 单个对象转换
     */
    public TARGET convert(SOURCE source, Class<TARGET> clazz) {
        if (source == null) {
            return null;
        }
        TARGET target = null;
        try {
            target = clazz.newInstance();
        } catch (Exception e) {
            logger.error("初始化{}对象失败。", clazz, e);
        }
        BeanHelper.copy(source, target);
        return target;
    }

    /**
     * 批量对象转换
     */
    public List<TARGET> convert(List<SOURCE> sourceList, Class<TARGET> clazz) {
        List<TARGET> targetList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(sourceList)) {
            for (SOURCE source : sourceList) {
                targetList.add(convert(source, clazz));
            }
        }
        return targetList;
    }
}

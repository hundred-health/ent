package com.theus.ent.core.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * JSON工具类
 * JSON框架：FastJSON
 * @author tangwei
 * @date 2020-04-29 13:47
 */
public class JsonUtils {

    /**
     * json字符串转实体
     * @param json json串
     * @param clazz 类
     * @param <T> 泛型
     * @return 实体类
     */
    public static <T> T jsonToObject(Object json, Class<T> clazz) {
        return JSONObject.parseObject(json.toString(), clazz);
    }

    /**
     * 实体转字符串
     * @param t 实体
     * @param <T> 泛型
     * @return json串
     */
    public static <T> String toJson(T t) {
        return JSON.toJSONString(t, SerializerFeature.WriteMapNullValue);
    }
}

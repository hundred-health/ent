package com.theus.ent.core.util;

import org.springframework.beans.BeanUtils;

/**
 * 实体工具
 *
 * @author tangwei
 * @date 2020-09-03 11:13
 */
public class BeanHelper {

    /**
     * 属性拷贝方法（浅拷贝）
     * @param source 源对象
     * @param target 目标对象
     */
    public static void copy(Object source, Object target) {
        BeanUtils.copyProperties(source, target);
    }
}

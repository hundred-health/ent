package com.theus.ent.base.service.sqlserver;


import com.theus.ent.base.model.dto.sqlserver.SysColumnsDTO;
import com.theus.ent.base.model.po.sqlserver.SysColumnsPO;

import java.util.List;

/**
 * @author tangwei
 * @date 2020-07-21 9:50
 */
public interface ColumnsService {

    /**
     * 查询列信息
     *
     * @param tableName 表名
     * @return list
     */
    List<SysColumnsDTO> findList(String tableName);
}

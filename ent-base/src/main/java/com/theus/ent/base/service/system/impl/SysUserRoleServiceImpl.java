package com.theus.ent.base.service.system.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.theus.ent.base.mapper.system.SysUserRoleMapper;
import com.theus.ent.base.model.po.system.SysRoleResource;
import com.theus.ent.base.model.po.system.SysUser;
import com.theus.ent.base.model.po.system.SysUserRole;
import com.theus.ent.base.service.system.SysUserRoleService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author tangwei
 * @date 2021-03-01 22:12
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {


    @Override
    public List<SysUserRole> findList(String roleId) {
        return this.baseMapper.selectList(new QueryWrapper<SysUserRole>().eq("role_id", roleId));
    }
}

package com.theus.ent.base.service.system;

import com.theus.ent.base.model.po.system.SysUser;

/**
 * 业务数据缓存处理
 *
 * @author tangwei
 * @date 2020-03-17 16:13
 */
public interface RedisService {

    /**
     * 获取用户缓存
     * @param account 用户账号
     * @return 用户
     */
    SysUser getUser(String account);

    /**
     * 添加用户缓存
     * @param account 用户账号
     * @param sysUser 用户
     */
    void addUser(String account, SysUser sysUser);

    /**
     * 移除用户缓存
     * @param account 用户账号
     */
    void removeUser(String account);
}

package com.theus.ent.base.service.system;

import com.baomidou.mybatisplus.extension.service.IService;
import com.theus.ent.base.model.po.system.SysUserRole;

import java.util.List;

/**
 * @author tangwei
 * @date 2021-03-01 22:12
 */
public interface SysUserRoleService extends IService<SysUserRole> {

    /**
     * 获取角色所属用户
     *
     * @param roleId 角色id
     * @return 用户list
     */
    List<SysUserRole> findList(String roleId);
}

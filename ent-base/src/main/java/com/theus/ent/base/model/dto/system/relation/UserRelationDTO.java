package com.theus.ent.base.model.dto.system.relation;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * 用户关系列表实体
 *
 * @author tangwei
 * @date 2021-07-13 11:21
 */
@Data
public class UserRelationDTO {

    /**
     * 用户关系编码
     */
    private String code;

    /**
     * 主用户编号
     */
    private String masterId;

    /**
     * 从用户编号
     */
    private String slaveId;

    /**
     * 用户账号
     */
    private String account;

    /**
     * 用户密码
     */
    private String password;

    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 手机号码
     */
    private String telephone;

    /**
     * 科室编码
     */
    private String deptId;

    /**
     * 科室名称
     */
    private String deptName;

    /**
     * 头衔编码
     */
    private String titleId;

    /**
     * 头衔名称
     */
    private String titleName;
}

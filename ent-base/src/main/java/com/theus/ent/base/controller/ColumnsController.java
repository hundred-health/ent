package com.theus.ent.base.controller;

import com.theus.ent.base.common.annotation.SysLogs;
import com.theus.ent.base.model.dto.sqlserver.SysColumnsDTO;
import com.theus.ent.base.service.sqlserver.ColumnsService;
import com.theus.ent.core.bean.ResponseCode;
import com.theus.ent.core.bean.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tangwei
 * @date 2021-07-04 9:54
 */
@RestController
@RequestMapping(value = "/metadata/columns")
@Api(tags = {"元数据管理"})
public class ColumnsController {
    @Resource
    private ColumnsService columnsService;

    @GetMapping(value = "/list/{tableName}")
    @ApiOperation(value = "获取表的列信息")
    @SysLogs("获取表的列信息")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<List<SysColumnsDTO>> getList(@PathVariable("tableName") @ApiParam("表名") String tableName) {
        return ResponseResult.e(ResponseCode.OK, columnsService.findList(tableName));
    }
}

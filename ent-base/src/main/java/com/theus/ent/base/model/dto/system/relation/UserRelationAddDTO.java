package com.theus.ent.base.model.dto.system.relation;

import lombok.Data;

/**
 * @author tangwei
 * @date 2021-07-13 20:35
 */
@Data
public class UserRelationAddDTO {

    /**
     * 从用户编号
     */
    private String slaveId;
}

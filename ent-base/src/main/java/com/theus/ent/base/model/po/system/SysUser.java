package com.theus.ent.base.model.po.system;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;
import java.util.List;

/**
 * 用户主表
 *
 * @author tangwei
 * @date 2020-09-02 15:00
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysUser implements Serializable  {

    /**
     * 用户ID
     */
    @TableId
    private String id;

    /**
     * 用户账号
     */
    private String account;

    /**
     * 用户密码
     */
    private String password;

    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 手机号码
     */
    private String telephone;

    /**
     * 科室编码
     */
    private String deptId;

    /**
     * 科室名称
     */
    @TableField(exist = false)
    private String deptName;

    /**
     * 头衔编码
     */
    private String titleId;

    /**
     * 头衔名称
     */
    @TableField(exist = false)
    private String titleName;

    /**
     * 用户email
     */
    private String email;

    /**
     * 用户身份证号码
     */
    private String identityNo;

    /**
     * 用户性别
     */
    private String sex;

    /**
     * 用户状态 0：禁用 1：正常
     */
    private Integer status;

    /**
     * 账号创建时间
     */
    private Date createTime;

    /**
     * 用户状态 0：正常 1：删除
     */
    @TableLogic
    private Integer delFlag;

    @TableField(exist = false)
    private List<SysRole> roles;

    @TableField(exist = false)
    private List<SysResource> resources;

    private static final long serialVersionUID = 1L;


}

package com.theus.ent.base.service.system;

import com.baomidou.mybatisplus.extension.service.IService;
import com.theus.ent.base.model.dto.system.dict.SysDictDTO;
import com.theus.ent.base.model.po.system.SysDict;
import com.theus.ent.base.model.vo.DictClassVO;
import com.theus.ent.base.model.vo.FindClassVO;

import java.util.List;

/**
 * @author tangwei
 * @date 2021-02-22 16:46
 */
public interface SysDictService extends IService<SysDict> {

    /**
     * 根据分类查询(先查redis，找不到查数据库）
     * @param classCode 字典分类代码
     * @return 字典信息
     */
    List<SysDictDTO> findByType(String classCode);

    /**
     * 查询字典分类树列表数据
     * @param findCatalogueVO 查询条件
     * @return list
     */
    List<DictClassVO> findDictClassTree(FindClassVO findCatalogueVO);

    /**
     * 获取字典项目分类
     * @return list
     */
    List<DictClassVO> findDictClasses();
}

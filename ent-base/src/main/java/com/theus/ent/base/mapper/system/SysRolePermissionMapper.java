package com.theus.ent.base.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.theus.ent.base.model.po.system.SysResource;
import com.theus.ent.base.model.po.system.SysRoleResource;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author tangwei
 * @date 2019-07-28 15:28
 */
@Mapper
@Repository
public interface SysRolePermissionMapper extends BaseMapper<SysRoleResource> {

    /**
     * 根据角色id数组获取授权的资源
     *
     * @param roleIds 角色id数组
     * @return 资源列表
     */
    List<SysResource> findResources(@Param(value="roleIds") String[] roleIds);
}
package com.theus.ent.base.model.dto.system.relation;

import lombok.Data;

/**
 * @author tangwei
 * @date 2021-07-13 11:17
 */
@Data
public class FindUserRelationDTO {

    /**
     * 姓名
     */
    private String realName;
}

package com.theus.ent.base.service.system;

import com.theus.ent.base.model.dto.system.relation.FindUserRelationDTO;
import com.theus.ent.base.model.dto.system.relation.UserRelationAddDTO;
import com.theus.ent.base.model.dto.system.relation.UserRelationDTO;
import com.theus.ent.base.model.po.system.SysUserRelation;

import java.util.List;

/**
 * @author tangwei
 * @date 2021-07-13 11:19
 */
public interface UserRelationService {

    /**
     * 查询未选择的用户
     *
     * @param findUserRelationDTO 条件
     * @return list
     */
    List<UserRelationDTO> queryUnselected(FindUserRelationDTO findUserRelationDTO);

    /**
     * 查询已选择的用户
     *
     * @param findUserRelationDTO 条件
     * @return list
     */
    List<UserRelationDTO> querySelected(FindUserRelationDTO findUserRelationDTO);

    /**
     * 删除已选择的用户关系
     *
     * @param code 关系编码
     */
    void removeUserRelation(String code);

    /**
     * 添加用户关系
     *
     * @param relationAddDTO 添加信息
     */
    void addUserRelation(UserRelationAddDTO relationAddDTO);
}

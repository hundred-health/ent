package com.theus.ent.base.service.system.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.theus.ent.base.mapper.system.UserRelationMapper;
import com.theus.ent.base.model.dto.system.relation.FindUserRelationDTO;
import com.theus.ent.base.model.dto.system.relation.UserRelationAddDTO;
import com.theus.ent.base.model.dto.system.relation.UserRelationDTO;
import com.theus.ent.base.model.po.system.SysUser;
import com.theus.ent.base.model.po.system.SysUserRelation;
import com.theus.ent.base.service.system.UserRelationService;
import com.theus.ent.base.util.ShiroUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author tangwei
 * @date 2021-07-13 11:26
 */
@Service
public class UserRelationServiceImpl extends ServiceImpl<UserRelationMapper, SysUserRelation> implements UserRelationService {
    private static Logger LOGGER = LoggerFactory.getLogger(UserRelationServiceImpl.class);

    @Override
    public List<UserRelationDTO> queryUnselected(FindUserRelationDTO findUserRelationDTO) {
        String masterId = ShiroUtils.getUser().getId();
        return this.baseMapper.queryUnselected(masterId, findUserRelationDTO.getRealName());
    }

    @Override
    public List<UserRelationDTO> querySelected(FindUserRelationDTO findUserRelationDTO) {
        String masterId = ShiroUtils.getUser().getId();
        return this.baseMapper.querySelected(masterId, findUserRelationDTO.getRealName());
    }

    @Override
    public void removeUserRelation(String code) {
        this.baseMapper.deleteById(code);
    }

    @Override
    public void addUserRelation(UserRelationAddDTO relationAddDTO) {
        String masterId = ShiroUtils.getUser().getId();
        SysUserRelation sysUserRelation = new SysUserRelation();
        sysUserRelation.setMasterId(masterId);
        sysUserRelation.setSlaveId(relationAddDTO.getSlaveId());
        SysUserRelation userRelation = this.baseMapper.queryRelation(masterId, relationAddDTO.getSlaveId());
        if (userRelation == null) {
            this.baseMapper.insert(sysUserRelation);
        }
    }
}

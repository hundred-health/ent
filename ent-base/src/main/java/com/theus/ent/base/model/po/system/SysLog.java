package com.theus.ent.base.model.po.system;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统日志
 *
 * @author tangwei
 * @date 2020-09-02 17:27
 */
@Data
public class SysLog implements Serializable {

    @TableId
    private String id;

    private String account;

    private String userId;

    private String ip;

    private Integer ajax;

    private String uri;

    private String params;

    private String httpMethod;

    private String classMethod;

    private String actionName;

    private Date createDate;


}

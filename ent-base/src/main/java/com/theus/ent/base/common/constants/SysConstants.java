package com.theus.ent.base.common.constants;

/**
 * @author tangwei
 * @date 2019-09-22 18:30
 */
public interface SysConstants {

    /**
     * 超级管理员用户名
     */
    String SUPER_ADMIN = "admin";

    /**
     * 超级管理员角色ID
     */
    String SUPER_ROLE_ID = "999999999";

    /**
     * 超级管理员角色名称
     */
    String SUPER_ROLE_NAME = "超级管理员";

    /**
     * 用户初始密码
     */
    String INITIAL_PASSWORD = "000000";

    /**
     * 用户起始账号
     */
    String START_ACCOUNT = "10001";

    /**
     * 顶级机构代码
     */
    String TOP_DEPT_CODE = "0";

    /**
     * 通用顶级代码
     */
    String TOP_COMMON_CODE = "0";

    /**
     * 真、假整数常量
     */
    interface TrueFalseInt {
        int TRUE = 1;

        int FALSE = 0;
    }

    /**
     * 操作类型
     */
    interface OperationType {
        int ADD = 1;

        int UPDATE = 2;
    }

    /**
     * 字符串起始索引
     */
    int STRING_START_INDEX = 0;

    /**
     * 状态
     */
    interface CommonsStatus {
        //禁用
        Integer JY = 0;
        //正常
        Integer ZC = 1;
    }
}
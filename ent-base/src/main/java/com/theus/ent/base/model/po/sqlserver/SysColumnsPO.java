package com.theus.ent.base.model.po.sqlserver;

import lombok.Data;

/**
 * 表列信息（元数据）
 *
 * @author tangwei
 * @date 2021-07-03 23:38
 */
@Data
public class SysColumnsPO {
    /**
     * 对象的ID。
     */
    private Integer objectId;

    /**
     * 列的名称。在对象内是唯一的
     */
    private String name;

    /**
     * 列的ID。在对象内是唯一的。列ID可能不是顺序的
     */
    private Integer columnId;

    /**
     * 列的系统类型的ID
     */
    private Short systemTypeId;

    /**
     * 用户定义的列类型的ID。
     */
    private Integer userTypeId;

    /**
     * 列的最大长度（以字节为单位）
     */
    private Short maxLength;

    /**
     * 如果列是数字，列的精度; 否则，0
     */
    private Short precision;

    /**
     * 如果列是数字,列的比例， 否则，0。
     */
    private Short scale;

    /**
     * 如果基于字符，则列的排序规则名称; 否则，为NULL。
     */
    private String collationName;

    /**
     * 1 =列可以为空
     */
    private Boolean isNullable;

    /**
     * 1 =列使用ANSI_PADDING ON行为（如果是字符，二进制或变体）。0 =列不是字符，二进制或变体。
     */
    private Boolean isAnsiPadded;

    /**
     * 1 = Column是声明的ROWGUIDCOL。
     */
    private Boolean isRowguidcol;

    /**
     * 1 =列具有标识值
     */
    private Boolean isIdentity;

    /**
     * 1 =列是计算列。
     */
    private Boolean isComputed;

    /**
     * 1 =列是FILESTREAM列
     */
    private Boolean isFilestream;

    /**
     * 1 =复制列
     */
    private Boolean isReplicated;

    /**
     * 1 =列具有非SQL Server订户
     */
    private Boolean isNonSqlSubscribed;

    /**
     * 1 =列已合并发布
     */
    private Boolean isMergePublished;

    /**
     * 1 =使用SSIS复制列
     */
    private Boolean isDtsReplicated;

    /**
     * 1 =内容是一个完整的XML文档。0 =内容是文档片段或列数据类型不是xml。
     */
    private Boolean isXmlDocument;

    /**
     * 如果列的数据类型是xml并且键入了XML，则为非零。该值将是包含列的验证XML架构命名空间的集合的ID。0 =无XML架构集合
     */
    private Boolean xmlCollectionId;

    /**
     * 默认对象的ID，无论它是独立对象sys.sp_bindefault还是内联列级DEFAULT约束。内联列级默认对象的parent_object_id列是返回表本身的引用。0 =无默认值。
     */
    private Boolean defaultObjectId;

    /**
     * 使用sys.sp_bindrule绑定到列的独立规则的ID。0 =没有独立的规则。有关列级CHECK约束，请参阅sys.check_constraints（Transact-SQL）
     */
    private Boolean ruleObjectId;

    /**
     * 1 =列是稀疏列
     */
    private Boolean isSparse;

    /**
     * 1 =列是列集
     */
    private Boolean isColumnSet;

    /**
     * 数据类型 关联sys.types
     */
    private String dataType;

    /**
     * 列描述 关联sys.extended_properties
     */
    private String columnDesc;
}

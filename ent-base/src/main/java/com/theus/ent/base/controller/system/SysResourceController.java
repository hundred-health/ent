package com.theus.ent.base.controller.system;

import com.theus.ent.base.common.annotation.SysLogs;
import com.theus.ent.base.common.constants.ResourceConstants;
import com.theus.ent.base.model.po.system.SysResource;
import com.theus.ent.base.service.system.SysResourceService;
import com.theus.ent.core.bean.ResponseCode;
import com.theus.ent.core.bean.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tangwei
 * @date 2020-10-19 17:29
 */
@RestController
@RequestMapping(value = "/system/resource")
@Api(tags = {"资源管理"})
public class SysResourceController {

    @Resource
    private SysResourceService resourceService;


    @GetMapping(value="/navTree/{username}")
    @ApiOperation(value = "获取用户菜单树")
    @SysLogs("获取用户菜单树")
    @ApiImplicitParam(paramType = "header",name = "Authorization",value = "身份认证Token")
    public ResponseResult<List<SysResource>> navTree(@PathVariable("username") @ApiParam("用户名") String username) {
        return ResponseResult.e(ResponseCode.OK,resourceService.findTree(username, ResourceConstants.ResultType.NO_BUTTON));
    }

    @PostMapping(value="/resTree/roles")
    @ApiOperation(value = "根据角色id集合获取资源树")
    @SysLogs("根据角色id集合获取资源树")
    @ApiImplicitParam(paramType = "header",name = "Authorization",value = "身份认证Token")
    public ResponseResult<List<SysResource>> resTreeByroles(@RequestBody @ApiParam("角色ID集合") String[] roles) {
        return ResponseResult.e(ResponseCode.OK,resourceService.findTree(roles));
    }

    @PostMapping(value = {"/list"})
    @ApiOperation(value = "获取所有的资源列表")
    @SysLogs("获取所有的资源列表")
    @ApiImplicitParam(paramType = "header",name = "Authorization",value = "身份认证Token")
    public ResponseResult<List<SysResource>> list(){
        return ResponseResult.e(ResponseCode.OK,resourceService.treeList());
    }
}

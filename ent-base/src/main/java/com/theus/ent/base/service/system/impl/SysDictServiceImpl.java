package com.theus.ent.base.service.system.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.theus.ent.base.common.constants.SysConstants;
import com.theus.ent.base.mapper.system.SysDictMapper;
import com.theus.ent.base.model.dto.system.dict.SysDictDTO;
import com.theus.ent.base.model.po.system.SysDict;
import com.theus.ent.base.model.po.system.SysDictClass;
import com.theus.ent.base.model.vo.DictClassVO;
import com.theus.ent.base.model.vo.FindClassVO;
import com.theus.ent.base.service.system.SysDictService;
import com.theus.ent.core.util.BaseConverter;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * @author tangwei
 * @date 2021-02-22 16:50
 */
@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements SysDictService {

    @Override
    public List<SysDictDTO> findByType(String classCode) {
        List<SysDict> sysDictList = this.baseMapper.selectList(
                new QueryWrapper<SysDict>()
                        .eq("class_code", classCode)
                        .select("class_code", "item_value", "item_name","sort")
                        .orderByAsc("sort"));
        List<SysDictDTO> dictDTOList = new BaseConverter<SysDict, SysDictDTO>().convert(sysDictList, SysDictDTO.class);
        // 按sort升序
        dictDTOList.sort(Comparator.comparingInt(SysDictDTO::getSort));
        return dictDTOList;
    }

    @Override
    public List<DictClassVO> findDictClassTree(FindClassVO findClassVO) {
        List<DictClassVO> dictClassVOList;
        List<SysDictClass> sysDictClassList;
        // 查询条件为空
        if (StrUtil.isBlank(findClassVO.getSearchText())) {
            sysDictClassList = this.baseMapper.findAllDictClasses();
            dictClassVOList = initTree(sysDictClassList);
        } else {
            sysDictClassList = this.baseMapper.findDictClassList(findClassVO);
            dictClassVOList = new BaseConverter<SysDictClass, DictClassVO>().convert(sysDictClassList, DictClassVO.class);
            findChildren(dictClassVOList, sysDictClassList);
            // 去除数据
            dictClassVOList = removeDuplicate(dictClassVOList);
        }
        return dictClassVOList;
    }

    @Override
    public List<DictClassVO> findDictClasses() {
        List<SysDictClass> sysDictClassList = this.baseMapper.findDictClasses();
        return new BaseConverter<SysDictClass, DictClassVO>().convert(sysDictClassList, DictClassVO.class);
    }

    /**
     * 根据字典分类列表生成字典分类树
     *
     * @param dictClassList 字典分类列表
     * @return 字典分类树
     */
    private List<DictClassVO> initTree(List<SysDictClass> dictClassList) {
        List<DictClassVO> list = new ArrayList<>();
        if (dictClassList != null) {
            // 循环字典分类列表
            for (SysDictClass sysDictClass : dictClassList) {
                // 保存顶级分类
                if (SysConstants.TOP_COMMON_CODE.equals(sysDictClass.getParentId())) {
                    DictClassVO dictClassVO = new DictClassVO();
                    BeanUtils.copyProperties(sysDictClass, dictClassVO);
                    list.add(dictClassVO);
                }
            }
            // 查找子分类
            findChildren(list, dictClassList);
        }
        return list;
    }

    /**
     * 根据上级字典分类和字典分类列表生成字典分类树
     *
     * @param dictClassVOS  上级字典分类list
     * @param dictClassList 字典分类列表
     */
    private void findChildren(List<DictClassVO> dictClassVOS, List<SysDictClass> dictClassList) {
        // 循环上级
        for (DictClassVO dictClassVO : dictClassVOS) {
            List<DictClassVO> children = new ArrayList<>();
            // 循环列表
            for (SysDictClass sysDictClass : dictClassList) {
                // 将分类列表中的父id是上级分类的id的分类存入子分类列表
                if (dictClassVO.getId().equals(sysDictClass.getParentId())) {
                    DictClassVO tempVO = new DictClassVO();
                    BeanUtils.copyProperties(sysDictClass, tempVO);
                    tempVO.setParentName(dictClassVO.getName());
                    children.add(tempVO);
                }
            }
            dictClassVO.setChildren(children);
            findChildren(children, dictClassList);
        }
    }

    private List<DictClassVO> removeDuplicate(List<DictClassVO> dictClassVOList) {
        List<DictClassVO> list = new ArrayList<>();
        // 循环
        for (DictClassVO dictClass : dictClassVOList) {
            boolean isExists = false;
            // 判断是否存在于其他字典分类的子集中
            for (DictClassVO other : dictClassVOList) {
                if (!dictClass.getId().equals(other.getId())) {
                    if (isExistsInList(dictClass, other.getChildren())) {
                        isExists = true;
                        break;
                    }
                }
            }
            // 若不存在与其他分类的子集中则保留
            if (!isExists) {
                list.add(dictClass);
            }
        }
        return list;
    }

    /**
     * 是否存在于列表
     *
     * @param dictClassVO   实体
     * @param dictClassList 实体list
     * @return 是否
     */
    private boolean isExistsInList(DictClassVO dictClassVO, List<DictClassVO> dictClassList) {
        boolean isExists = false;
        if (dictClassList != null) {
            for (DictClassVO other : dictClassList) {
                if (dictClassVO.getId().equals(other.getId())) {
                    isExists = true;
                    break;
                }
                if (!isExists) {
                    isExists = isExistsInList(dictClassVO, other.getChildren());
                }
            }
        }
        return isExists;
    }
}

package com.theus.ent.base.controller.system;

import com.theus.ent.base.common.controller.QueryController;
import com.theus.ent.base.model.dto.system.log.FindLogDTO;
import com.theus.ent.base.model.po.system.SysLog;
import com.theus.ent.base.service.system.SysLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tangwei
 * @date 2019-08-24 22:43
 */
@RestController
@RequestMapping(value = "/system/log")
@Api(tags = {"日志管理"})
public class SysLogController implements QueryController<SysLog, FindLogDTO, SysLogService> {

    @Resource
    private SysLogService sysLogService;

    @Override
    public SysLogService getService() {
        return sysLogService;
    }


}

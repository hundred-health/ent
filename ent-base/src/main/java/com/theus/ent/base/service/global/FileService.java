package com.theus.ent.base.service.global;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author tangwei
 * @date 2020-09-04 15:26
 */
public interface FileService {

    /**
     * 上传图片
     * @param request 请求
     */
    List<String> uploadImage(HttpServletRequest request);
}

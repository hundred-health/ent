package com.theus.ent.base.model.dto.system.user;

import lombok.Data;

/**
 * @author tangwei
 * @date 2020-03-18 17:50
 */
@Data
public class UserDTO {
    private String id;
    private String account;
    private String email;
    private String realName;
    private String mobile;
    private String organCode;
    private String organName;
    private String telephone;
}

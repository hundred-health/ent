package com.theus.ent.base.controller;

import com.theus.ent.base.service.global.FileService;
import com.theus.ent.core.bean.ResponseCode;
import com.theus.ent.core.bean.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 文件上传控制器
 *
 * @author tangwei
 * @date 2020-09-04 15:21
 */
@RestController
@RequestMapping("/file")
@Api(tags = {"上传管理"})
public class FileController {

    @Resource
    private FileService fileService;

    @RequestMapping(value = "/image", method = RequestMethod.POST)
    @ApiOperation(value = "上传图片")
    @ResponseBody
    @ApiImplicitParam(paramType = "header",name = "Authorization",value = "身份认证Token")
    //@SysLogs("上传图片")
    public ResponseResult<List<String>> uploadImage(HttpServletRequest request) {
        return ResponseResult.e(ResponseCode.OK, fileService.uploadImage(request));
    }
}

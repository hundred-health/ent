package com.theus.ent.base.model.vo;

import com.theus.ent.base.model.po.system.SysResource;
import com.theus.ent.base.model.po.system.SysRole;
import lombok.Data;

import java.sql.Blob;
import java.util.Date;
import java.util.List;

/**
 * @author tangwei
 * @date 2019-07-28 21:48
 */
@Data
public class SysUserVO {
    private String id;

    /**
     * 用户账号
     */
    private String account;

    /**
     * 用户密码
     */
    private String password;

    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 手机号码
     */
    private String telephone;

    /**
     * 科室编码
     */
    private String deptId;

    /**
     * 头衔编码
     */
    private String titleId;

    /**
     * 用户email
     */
    private String email;

    /**
     * 用户身份证号码
     */
    private String identityNo;

    /**
     * 用户性别
     */
    private String sex;

    /**
     * 用户状态 0：禁用 1：正常
     */
    private Integer status;

    /**
     * 账号创建时间
     */
    private Date createTime;

    /**
     * 用户角色
     */
    private List<SysRole> roles;

    /**
     * 用户资源
     */
    private List<SysResource> resources;
}

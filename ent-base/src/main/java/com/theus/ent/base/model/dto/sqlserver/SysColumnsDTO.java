package com.theus.ent.base.model.dto.sqlserver;

import lombok.Data;

/**
 * 表列信息（元数据）
 *
 * @author tangwei
 * @date 2021-07-03 0:00
 */
@Data
public class SysColumnsDTO {

    /**
     * 列的名称。在对象内是唯一的
     */
    private String name;

    /**
     * 列的ID。在对象内是唯一的。列ID可能不是顺序的
     */
    private Integer columnId;

    /**
     * 列的最大长度（以字节为单位）
     */
    private Short maxLength;

    /**
     * 如果列是数字，列的精度; 否则，0
     */
    private Short precision;

    /**
     * 如果列是数字,列的比例， 否则，0。
     */
    private Short scale;

    /**
     * 1 =列可以为空
     */
    private Boolean isNullable;

    /**
     * 数据类型 关联sys.types
     */
    private String dataType;

    /**
     * 列描述 关联sys.extended_properties
     */
    private String columnDesc;
}

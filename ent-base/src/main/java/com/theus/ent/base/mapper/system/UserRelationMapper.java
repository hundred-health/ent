package com.theus.ent.base.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.theus.ent.base.model.dto.system.relation.FindUserRelationDTO;
import com.theus.ent.base.model.dto.system.relation.UserRelationDTO;
import com.theus.ent.base.model.po.system.SysUserRelation;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author tangwei
 * @date 2021-07-13 11:28
 */
@Mapper
@Repository
public interface UserRelationMapper extends BaseMapper<SysUserRelation> {

    /**
     * 获取已选择的用户
     *
     * @param masterId 主用户编号
     * @param realName 姓名
     * @return list
     */
    List<UserRelationDTO> queryUnselected(@Param("masterId") String masterId,@Param("realName") String realName);

    /**
     * 获取已选择的用户
     *
     * @param masterId 主用户编号
     * @param realName 姓名
     * @return list
     */
    List<UserRelationDTO> querySelected(@Param("masterId") String masterId,@Param("realName") String realName);

    /**
     * 查询用户关系
     *
     * @param masterId 主用户id
     * @param slaveId 从用户id
     * @return 用户关系
     */
    SysUserRelation queryRelation(@Param("masterId") String masterId,@Param("slaveId") String slaveId);
}

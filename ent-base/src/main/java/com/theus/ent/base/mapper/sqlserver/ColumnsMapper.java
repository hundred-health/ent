package com.theus.ent.base.mapper.sqlserver;

import com.theus.ent.base.model.po.sqlserver.SysColumnsPO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author tangwei
 * @date 2021-07-03 23:46
 */
@Mapper
@Repository
public interface ColumnsMapper {

    /**
     * 查询表的列信息
     *
     * @param tableName 表
     * @return 列信息
     */
    List<SysColumnsPO> findList(String tableName);
}

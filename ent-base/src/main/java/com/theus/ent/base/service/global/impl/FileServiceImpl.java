package com.theus.ent.base.service.global.impl;

import com.theus.ent.base.service.global.FileService;
import com.theus.ent.core.model.UploadFile;
import com.theus.ent.core.util.Uploader;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author tangwei
 * @date 2020-09-04 15:26
 */
@Service
public class FileServiceImpl implements FileService {

    @Override
    public List<String> uploadImage(HttpServletRequest request) {
        Uploader uploader = new Uploader(request);
        List<UploadFile> uploadFileList = uploader.upload();
        return uploadFileList.stream().filter(v -> v.getSuccess() == 1).map(UploadFile::getUrl).collect(Collectors.toList());
    }
}

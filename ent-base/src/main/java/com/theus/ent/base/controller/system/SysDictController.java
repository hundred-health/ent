package com.theus.ent.base.controller.system;

import com.theus.ent.base.common.annotation.SysLogs;
import com.theus.ent.base.model.dto.system.dict.SysDictDTO;
import com.theus.ent.base.model.vo.DictClassVO;
import com.theus.ent.base.model.vo.FindClassVO;
import com.theus.ent.base.service.system.SysDictService;
import com.theus.ent.core.bean.ResponseCode;
import com.theus.ent.core.bean.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tangwei
 * @date 2021-02-22 16:43
 */
@Api(tags = {"字典管理"})
@RestController
@RequestMapping("/system/dict")
public class SysDictController {

    @Resource
    private SysDictService sysDictService;

    @GetMapping(value="/items/{classCode}")
    @ApiOperation(value = "根据字典分类代码获取字典项信息")
    @SysLogs("根据字典分类代码获取字典项信息")
    @ApiImplicitParam(paramType = "header",name = "Authorization",value = "身份认证Token")
    public ResponseResult<List<SysDictDTO>> findByType(@PathVariable("classCode") @ApiParam("字典分类代码") String classCode) {
        return ResponseResult.e(ResponseCode.OK,sysDictService.findByType(classCode));
    }

    @PostMapping(value = {"/classTree"})
    @ApiOperation(value = "获取字典分类树")
    @SysLogs("获取字典分类树")
    @ApiImplicitParam(paramType = "header",name = "Authorization",value = "身份认证Token")
    public ResponseResult<List<DictClassVO>> findDictClassTree(@RequestBody @Validated @ApiParam(value = "字典分类查询条件") FindClassVO findCatalogueVO){
        return ResponseResult.e(ResponseCode.OK,sysDictService.findDictClassTree(findCatalogueVO));
    }

    @GetMapping(value = {"/classes"})
    @ApiOperation(value = "获取字典项目分类")
    @SysLogs("获取字典项目分类")
    @ApiImplicitParam(paramType = "header",name = "Authorization",value = "身份认证Token")
    public ResponseResult<List<DictClassVO>> findDictClasses(){
        return ResponseResult.e(ResponseCode.OK,sysDictService.findDictClasses());
    }
}

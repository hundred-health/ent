package com.theus.ent.base.service.system;

import com.baomidou.mybatisplus.extension.service.IService;
import com.theus.ent.base.model.po.system.SysResource;
import com.theus.ent.base.model.po.system.SysRoleResource;

import java.util.List;

/**
 * @author tangwei
 * @date 2021-03-01 22:08
 */
public interface SysRoleResourceService extends IService<SysRoleResource> {

    /**
     * 根据角色id获取对应资源权限
     *
     * @param rid 角色id
     * @return 资源权限
     */
    List<SysResource> findAllResourceByRoleId(String rid);

    /**
     * 根据角色id数组获取授权的资源
     *
     * @param roleIds 角色id数组
     * @return 资源列表
     */
    List<SysResource> findResources(String[] roleIds);
    /**
     * 保存角色资源
     * @param records 资源
     */
    void saveRoleResource(List<SysRoleResource> records);
}

package com.theus.ent.base.model.dto.system.user;

import com.theus.ent.base.model.dto.BaseSplitPageDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author tangwei
 * @date 2019-08-11 20:25
 */
@Data
@EqualsAndHashCode(callSuper=true)
public class FindUserDTO extends BaseSplitPageDTO {

    /**
     * 用户账号
     */
    private String account;

    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 手机号码
     */
    private String telephone;
}

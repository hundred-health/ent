package com.theus.ent.base.model.dto.system.user;

import com.theus.ent.base.model.po.system.SysRole;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author tangwei
 * @date 2019-07-28 21:55
 */
@Data
public class UserAddDTO {
    private String account;

    private String realName;

    private String password;

    @NotBlank(message = "手机号码不能为空！")
    private String telephone;

    private String deptId;

    private String titleId;

    private List<SysRole> roles;
}

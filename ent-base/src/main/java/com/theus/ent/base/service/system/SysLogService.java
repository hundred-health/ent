package com.theus.ent.base.service.system;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.theus.ent.base.common.service.QueryService;
import com.theus.ent.base.model.dto.system.log.FindLogDTO;
import com.theus.ent.base.model.po.system.SysLog;

import java.util.List;

/**
 * @author tangwei
 * @date 2020-09-02 17:28
 */
public interface SysLogService extends IService<SysLog>, QueryService<SysLog, FindLogDTO> {

    /**
     * 查询列表（分页）
     *
     * @param findLogDTO 条件
     * @return 日志分页信息
     */
    @Override
    IPage<SysLog> list(FindLogDTO findLogDTO);
}

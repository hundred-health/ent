package com.theus.ent.base.util;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

/**
 * @author tangwei
 * @date 2021-02-03 11:09
 */
public class HttpUtil {

    public static String sendGetRequest(String url) {
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(url, String.class);
        return result;
    }

    public static String sendPostRequest(String url, HttpMethod method, HttpEntity<Map<String, Object>> httpEntity) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.exchange(url, method, httpEntity, String.class);
        return response.getBody();
    }
}

package com.theus.ent.base.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.theus.ent.base.model.po.system.SysDict;
import com.theus.ent.base.model.po.system.SysDictClass;
import com.theus.ent.base.model.vo.DictClassVO;
import com.theus.ent.base.model.vo.FindClassVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author tangwei
 * @date 2019-10-07 22:18
 */
@Mapper
@Repository
public interface SysDictMapper extends BaseMapper<SysDict> {

    /**
     * 查询所有字典分类数据
     * @return 字典分类list
     */
    List<SysDictClass> findAllDictClasses();

    /**
     * * 查询字典分类数据
     * @param findClassVO 条件
     * @return 字典分类list
     */
    List<SysDictClass> findDictClassList(@Param("param") FindClassVO findClassVO);

    /**
     * 获取字典项目分类
     * @return list
     */
    List<SysDictClass> findDictClasses();
}


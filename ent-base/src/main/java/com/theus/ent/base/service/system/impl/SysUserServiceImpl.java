package com.theus.ent.base.service.system.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.theus.ent.base.common.constants.DictConstants;
import com.theus.ent.base.common.constants.ResourceConstants;
import com.theus.ent.base.common.constants.SysConstants;
import com.theus.ent.base.config.jwt.JwtToken;
import com.theus.ent.base.mapper.system.SysUserMapper;
import com.theus.ent.base.model.dto.SignInDTO;
import com.theus.ent.base.model.dto.system.user.*;
import com.theus.ent.base.model.po.system.SysResource;
import com.theus.ent.base.model.po.system.SysRole;
import com.theus.ent.base.model.po.system.SysUser;
import com.theus.ent.base.model.po.system.SysUserRole;
import com.theus.ent.base.model.vo.SysUserVO;
import com.theus.ent.base.service.global.ShiroService;
import com.theus.ent.base.service.system.*;
import com.theus.ent.base.util.LoginUtil;
import com.theus.ent.base.util.ShiroUtils;
import com.theus.ent.core.bean.ResponseCode;
import com.theus.ent.core.exception.BusinessException;
import com.theus.ent.core.util.BaseConverter;
import com.theus.ent.core.util.Encrypt;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author tangwei
 * @date 2020-09-02 16:00
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {
    private static Logger LOGGER = LoggerFactory.getLogger(SysUserServiceImpl.class);

    @Resource
    private SysRoleService roleService;
    @Resource
    private SysUserRoleService userRoleService;
    @Resource
    private SysResourceService resourceService;
    @Resource
    private ShiroService shiroService;
    @Resource
    private RedisService redisService;

    @Override
    public SysUser findUser(String account, boolean hasResource) {
        SysUser user = this.getCacheUser(account);
        if (user == null) {
            return null;
        }
        if (account.equals(SysConstants.SUPER_ADMIN)) {
            List<SysRole> roleList = new ArrayList<>();
            roleList.add(roleService.getSuperRole());
            user.setRoles(roleList);
        } else {
            user.setRoles(roleService.findAllRoleByUserId(user.getId(), hasResource));
        }
        return user;
    }

    @Override
    public SysUser findUserById(String id, boolean hasResource) {
        SysUser user = this.getById(id);
        if (user == null) {
            return null;
        }
        user.setRoles(roleService.findAllRoleByUserId(user.getId(), false));
        return user;
    }

    @Override
    public SysUser findUserByPhone(String telephone) {
        return this.baseMapper.selectOne(new QueryWrapper<SysUser>().eq("telephone", telephone));
    }

    @Override
    public UserDTO getUserInfo(String account) {
        if (StrUtil.isNotBlank(account)) {
            SysUser sysUser = this.getCacheUser(account);
            return new BaseConverter<SysUser, UserDTO>().convert(sysUser, UserDTO.class);
        } else {
            return null;
        }
    }

    @Override
    public void signIn(SignInDTO signInDTO) {
        if ("".equals(signInDTO.getUsername()) || "".equals(signInDTO.getPassword())) {
            throw new BusinessException(ResponseCode.SING_IN_INPUT_EMPTY);
        }
        JwtToken token = new JwtToken(null, signInDTO.getUsername(), signInDTO.getPassword());
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.login(token);
            if (!subject.isAuthenticated()) {
                throw new BusinessException(ResponseCode.SIGN_IN_INPUT_FAIL);
            }
        } catch (DisabledAccountException e) {
            throw new BusinessException(ResponseCode.SIGN_IN_INPUT_FAIL.code, e.getMessage(), e);
        } catch (Exception e) {
            throw new BusinessException(ResponseCode.SIGN_IN_FAIL, e);
        }
    }

    @Override
    public SysUserVO getCurrentUser() {
        HttpServletRequest request =
                ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        boolean b = LoginUtil.executeLogin(request);
        if (!b) {
            throw BusinessException.fail("身份已过期或无效，请重新认证");
        }
        Subject subject = SecurityUtils.getSubject();
        if (!subject.isAuthenticated()) {
            throw new BusinessException(ResponseCode.NOT_SING_IN);
        }
        JwtToken jwtToken = new JwtToken();
        Object principal = subject.getPrincipal();
        if (principal == null) {
            throw BusinessException.fail("用户信息获取失败");
        }
        BeanUtils.copyProperties(principal, jwtToken);
        SysUser user = this.findUser(jwtToken.getUsername(), false);
        if (user == null) {
            throw BusinessException.fail("用户不存在");
        }
        //获取菜单/权限信息
        List<SysResource> allPer = userRolesRegexResource(roleService.findAllRoleByUserId(user.getId(), true));
        SysUserVO vo = new SysUserVO();
        BeanUtils.copyProperties(user, vo);
        vo.setResources(allPer);
        return vo;
    }

    @Override
    public List<SysResource> userRolesRegexResource(List<SysRole> roles) {
        if (roles != null && roles.size() > 0) {
            Map<String, SysResource> resourceMap = new LinkedHashMap<>();
            roles.forEach(role -> {
                if (role.getResources() != null && role.getResources().size() > 0) {
                    //含有则不覆盖
                    role.getResources().forEach(resource ->
                            resourceMap.putIfAbsent(resource.getId(), resource));
                }
            });
            Map<String, SysResource> cacheMap = new ConcurrentHashMap<>(16);
            List<SysResource> resourceList = new CopyOnWriteArrayList<>();
            resourceMap.forEach((k, v) -> {
                SysResource allParent = resourceService.getResourceAllParent(v, cacheMap, resourceMap);
                //判断是否已经包含此对象
                if (!resourceList.contains(allParent)) {
                    resourceList.add(allParent);
                }
            });
            return resourceList;
        }
        return null;
    }

    @Override
    public SysUser getCacheUser(String account) {
        SysUser sysUser = this.redisService.getUser(account);
        // 未获取到用户 则从数据库中获取
        if (sysUser == null) {
            sysUser = this.baseMapper.getUser(account);
            // 缓存用户信息
            this.redisService.addUser(account, sysUser);
        }
        return sysUser;
    }

    @Override
    public IPage<SysUser> findPage(FindUserDTO findUserDTO) {
        IPage<SysUser> userPage = new Page<>(findUserDTO.getPageNum(),
                findUserDTO.getPageSize());
        // 获取用户
        userPage.setRecords(this.baseMapper.findPage(userPage, findUserDTO));
        userPage.getRecords().forEach(v -> {
            // 查找匹配所有用户的角色
            v.setRoles(roleService.findAllRoleByUserId(v.getId(), false));
            // 获取用户资源树
            v.setResources(resourceService.findTree(v.getAccount(), ResourceConstants.ResultType.WITH_BUTTON));
        });
        return userPage;
    }

    @Override
    public void statusChange(String userId, Integer status) {
        SysUser user = this.getById(userId);
        if (user == null) {
            throw BusinessException.fail("用户不存在");
        }
        SysUser sysUser = ShiroUtils.getUser();
        if (user.getAccount().equals(sysUser.getAccount())) {
            throw BusinessException.fail("不能锁定自己的账户");
        }
        user.setStatus(status);
        try {
            this.updateById(user);
            shiroService.clearAuthByUserId(userId, true, true);
            redisService.removeUser(user.getAccount());
        } catch (Exception e) {
            throw BusinessException.fail("操作失败", e);
        }
    }

    @Override
    public void removeUser(String userId) {
        SysUser user = this.getById(userId);
        if (user == null) {
            throw BusinessException.fail("用户不存在！");
        }
        SysUser sysUser = ShiroUtils.getUser();
        if (user.getAccount().equals(sysUser.getAccount())) {
            throw BusinessException.fail("不能删除自己的账户！");
        }
        try {
            // 删除用户角色关系
            userRoleService.remove(new QueryWrapper<SysUserRole>().eq("user_id", user.getId()));
            this.removeById(userId);
            shiroService.clearAuthByUserId(userId, true, true);
            redisService.removeUser(user.getAccount());
        } catch (Exception e) {
            throw BusinessException.fail("删除失败", e);
        }
    }

    @Override
    public void add(UserAddDTO addDTO) {
        SysUser findUser = this.findUserByPhone(addDTO.getTelephone());
        if (findUser != null) {
            throw BusinessException.fail(String.format("已经存在手机号码为 %s 的用户", addDTO.getTelephone()));
        }
        try {
            findUser = new SysUser();
            BeanUtils.copyProperties(addDTO, findUser);
            findUser.setAccount(this.createUserAccount());
            findUser.setCreateTime(new Date());
            // 初始化密码
            findUser.setPassword(SysConstants.INITIAL_PASSWORD);
            // 加密密码
            findUser.setPassword(Encrypt.md5(findUser.getPassword() + findUser.getAccount()));
            findUser.setStatus(DictConstants.UnLockStatus.YES.getValue());
            this.save(findUser);
            this.updateUserRole(findUser);
        } catch (Exception e) {
            throw BusinessException.fail("添加用户失败", e);
        }
    }

    @Override
    public void update(String id, UserUpdateDTO updateDTO) {
        SysUser user = this.getById(id);
        if (user == null) {
            throw BusinessException.fail(
                    String.format("更新失败，不存在ID为 %s 的用户", id));
        }
        if (SysConstants.SUPER_ADMIN.equalsIgnoreCase(user.getAccount())) {
            throw BusinessException.fail("超级管理员不允许修改!");
        }
        SysUser findUser = this.getOne(new QueryWrapper<SysUser>()
                .eq("account", updateDTO.getAccount()).ne("id", id));
        if (findUser != null) {
            throw BusinessException.fail(
                    String.format("更新失败，已经存在用户名为 %s 的用户", updateDTO.getAccount()));
        }
        findUser = this.getOne(new QueryWrapper<SysUser>()
                .eq("telephone", updateDTO.getTelephone()).ne("id", id));
        if (findUser != null) {
            throw BusinessException.fail(
                    String.format("更新失败，已经存在手机号为 %s 的用户", updateDTO.getTelephone()));
        }
        // 修改用户, 且修改了密码
        if (!updateDTO.getPassword().equals(user.getPassword())
                && !updateDTO.getPassword().equals(Encrypt.md5(user.getPassword() + user.getAccount()))) {
            updateDTO.setPassword(Encrypt.md5(updateDTO.getPassword() + updateDTO.getAccount()));
        }
        BeanUtils.copyProperties(updateDTO, user);
        try {
            this.updateById(user);
            this.updateUserRole(user);
            shiroService.clearAuthByUserId(user.getId(), true, false);
            redisService.removeUser(user.getAccount());
        } catch (BusinessException e) {
            throw BusinessException.fail(e.getMsg(), e);
        } catch (Exception e) {
            throw BusinessException.fail("用户信息更新失败", e);
        }
    }

    @Override
    public void updateUserRole(SysUser user) {
        try {
            userRoleService.remove(new QueryWrapper<SysUserRole>().eq("user_id", user.getId()));
            if (user.getRoles() != null && user.getRoles().size() > 0) {
                user.getRoles().forEach(v -> userRoleService.save(SysUserRole.builder()
                        .userId(user.getId())
                        .roleId(v.getId()).build()));
            }
        } catch (Exception e) {
            throw BusinessException.fail("用户权限关联失败", e);
        }
    }

    @Override
    public void resetPassword(ResetPasswordDTO resetPasswordDTO) {
        SysUser user = this.getById(resetPasswordDTO.getUid().trim());
        if (user == null) {
            throw BusinessException.fail(String.format("不存在ID为 %s 的用户", resetPasswordDTO.getUid()));
        }
        String password = Encrypt.md5(resetPasswordDTO.getPassword() + user.getAccount());
        user.setPassword(password);
        try {
            this.updateById(user);
            shiroService.clearAuthByUserId(user.getId(), true, true);
            redisService.removeUser(user.getAccount());
        } catch (Exception e) {
            throw BusinessException.fail(String.format("ID为 %s 的用户密码重置失败", resetPasswordDTO.getUid()), e);
        }
    }

    @Override
    public Set<String> findPermissions(String userName) {
        Set<String> perms = new HashSet<>();
        List<SysResource> sysResources = resourceService.findByUser(userName);
        for (SysResource sysResource : sysResources) {
            perms.add(sysResource.getPermission());
        }
        return perms;
    }

    /**
     * 生成用户账号
     *
     * @return 用户账号
     */
    private String createUserAccount(){
        String maxAccount = this.baseMapper.getMaxAccount();
        if(StrUtil.isNotBlank(maxAccount)){
            return String.valueOf(Math.addExact(Integer.parseInt(maxAccount), 1));
        }else{
            return SysConstants.START_ACCOUNT;
        }
    }

    public List<SysUser> list(FindUserDTO findUserDTO){

        QueryWrapper<SysUser> sysUserWrapper = new QueryWrapper<>();

        if(StrUtil.isNotBlank(findUserDTO.getRealName())) {

            sysUserWrapper.eq("real_name", findUserDTO.getRealName());
        }

        return baseMapper.selectList(sysUserWrapper);
    }
}

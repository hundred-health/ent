package com.theus.ent.base.model.po.system;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import org.springframework.data.annotation.Id;

/**
 * 用户权限关系表
 *
 * @author tangwei
 * @date 2021-07-13 11:15
 */
@Data
public class SysUserRelation {

    /**
     * 关系编号
     */
    @TableId
    private String code;

    /**
     * 主用户编号
     */
    private String masterId;

    /**
     * 从用户编号
     */
    private String slaveId;
}

package com.theus.ent.base.model.po.system;

import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;

import java.util.Date;

/**
 * 基础模型
 * @author tangwei
 * @date 2021-03-08 20:42
 */
@Data
public class BaseModel {

    private String createUserId;

    private String createUserName;

    private Date createTime;

    private String updateUserId;

    private String updateUserName;

    private Date updateTime;

    @TableLogic
    private Byte delFlag;
}

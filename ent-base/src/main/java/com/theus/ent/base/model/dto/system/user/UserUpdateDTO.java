package com.theus.ent.base.model.dto.system.user;

import com.theus.ent.base.model.po.system.SysRole;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author tangwei
 * @version 2019/8/24/21:50
 */
@Data
public class UserUpdateDTO {
    private String id;

    private String realName;

    private String account;

    private String password;

    private Integer age;

    private String email;

    @NotBlank(message = "手机号码不能为空！")
    private String telephone;

    private String mobile;

    private String deptId;

    private String deptName;

    private String titleId;

    @NotNull(message = "状态标识不能为空")
    private Integer status;

    @Size(min = 1, message = "请至少选择一个角色")
    private List<SysRole> roles;

}

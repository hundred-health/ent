package com.theus.ent.base.service.sqlserver.impl;

import com.theus.ent.base.mapper.sqlserver.ColumnsMapper;
import com.theus.ent.base.model.dto.sqlserver.SysColumnsDTO;
import com.theus.ent.base.model.po.sqlserver.SysColumnsPO;
import com.theus.ent.base.service.sqlserver.ColumnsService;
import com.theus.ent.base.service.system.impl.SysUserServiceImpl;
import com.theus.ent.core.util.BaseConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author tangwei
 * @date 2020-07-21 9:50
 */
@Service
public class ColumnsServiceImpl implements ColumnsService {

    private static Logger LOGGER = LoggerFactory.getLogger(ColumnsServiceImpl.class);
    @Resource
    private ColumnsMapper columnsMapper;

    @Override
    public List<SysColumnsDTO> findList(String tableName) {
        List<SysColumnsPO> sysColumnsList = columnsMapper.findList(tableName);
        return new BaseConverter<SysColumnsPO,SysColumnsDTO>().convert(sysColumnsList,SysColumnsDTO.class);
    }
}

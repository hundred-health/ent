package com.theus.ent.base.service.system.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.theus.ent.base.mapper.system.SysLogMapper;
import com.theus.ent.base.model.dto.system.log.FindLogDTO;
import com.theus.ent.base.model.po.system.SysLog;
import com.theus.ent.base.service.system.SysLogService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author tangwei
 * @date 2020-09-02 17:28
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements SysLogService {

    @Override
    public IPage<SysLog> list(FindLogDTO findLogDTO) {
        QueryWrapper<SysLog> wrapper = new QueryWrapper<>();
        if (StrUtil.isNotBlank(findLogDTO.getName())) {
            wrapper.eq("account",findLogDTO.getName())
                    .orderBy(true, findLogDTO.getAsc(), "create_date");
        }
        return this.page(new Page<>(findLogDTO.getPageNum(), findLogDTO.getPageSize()), wrapper);
    }
}

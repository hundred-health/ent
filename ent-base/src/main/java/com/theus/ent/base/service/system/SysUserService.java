package com.theus.ent.base.service.system;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.theus.ent.base.model.dto.SignInDTO;
import com.theus.ent.base.model.dto.system.user.*;
import com.theus.ent.base.model.po.system.SysResource;
import com.theus.ent.base.model.po.system.SysRole;
import com.theus.ent.base.model.po.system.SysUser;
import com.theus.ent.base.model.vo.SysUserVO;

import java.util.List;
import java.util.Set;

/**
 * @author tangwei
 * @date 2020-09-02 16:00
 */
public interface SysUserService extends IService<SysUser> {

    /**
     * 根据账号查找用户信息
     * @param account 账号
     * @param hasResource 是否包含权限
     * @return User
     */
    SysUser findUser(String account, boolean hasResource);

    /**
     * 根据ID查找用户
     * @param id ID
     * @param hasResource 是否包含权限
     * @return User
     */
    SysUser findUserById(String id, boolean hasResource);

    /**
     * 根据手机号查找用户
     *
     * @param telephone 手机号
     * @return 用户
     */
    SysUser findUserByPhone(String telephone);

    /**
     * 根据用户名查找用户
     * @param account 账户名
     * @return 用户
     */
    UserDTO getUserInfo(String account);

    /**
     * 用户登录操作
     * @param signInDTO 登录信息
     */
    void signIn(SignInDTO signInDTO);

    /**
     * 获取当前登录用户信息
     * @return UserVO
     */
    SysUserVO getCurrentUser();

    /**
     * 用户角色资源匹配
     * @param roles 用户角色集
     * @return 资源集合
     */
    List<SysResource> userRolesRegexResource(List<SysRole> roles);

    /**
     * 根据账号查找用户
     * @param account 账号名
     * @return 用户
     */
    SysUser getCacheUser(String account);

    /**
     * 查询用户（分页）
     * @param findUserDTO 过滤条件
     * @return RequestResult
     */
    IPage<SysUser> findPage(FindUserDTO findUserDTO);

    /**
     * 用户状态改变
     * @param userId 用户ID
     * @param status 状态码
     */
    void statusChange(String userId, Integer status);

    /**
     * 删除用户
     * @param userId 用户ID
     */
    void removeUser(String userId);

    /**
     * 添加用户
     * @param addDTO 用户数据DTO
     */
    void add(UserAddDTO addDTO);

    /**
     * 更新用户数据
     * @param id 用户id
     * @param updateDTO 用户数据DTO
     */
    void update(String id, UserUpdateDTO updateDTO);

    /**
     * 更新用户角色关联
     * @param user 用户数据
     */
    void updateUserRole(SysUser user);

    /**
     * 重置用户密码
     * @param resetPasswordDTO 重置密码信息
     */
    void resetPassword(ResetPasswordDTO resetPasswordDTO);

    /**
     *查找用户的菜单权限标识集合
     * @param username 用户名
     * @return 菜单权限标识集合
     */
    Set<String> findPermissions(String username);

    /**
     * 查询所有的用户信息
     * @return
     */
    List<SysUser> list(FindUserDTO findUserDTO);

}

package com.theus.ent.base.config.shiro;

import com.theus.ent.base.config.jwt.JwtToken;
import com.theus.ent.base.util.JwtUtil;
import com.theus.ent.core.util.Encrypt;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;

/**
 * @author tangwei
 * @version 2019/08/03/22:32
 */
public class CredentialsMatcher extends SimpleCredentialsMatcher {

    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        JwtToken jwtToken = (JwtToken) token;
        Object accountCredentials = getCredentials(info);
        if(jwtToken.getPassword()!=null){
            Object tokenCredentials = Encrypt.md5(String.valueOf(
                    jwtToken.getPassword())+jwtToken.getUsername());
            if(!accountCredentials.equals(tokenCredentials)){
                throw new DisabledAccountException("密码不正确！");
            }
        }else{
            boolean verify = JwtUtil.verify(jwtToken.getToken(), jwtToken.getUsername(), accountCredentials.toString());
            if(!verify){
                throw new DisabledAccountException("verifyFail");
            }
        }
        return true;
    }

    /**
     * 获取密码
     * @param args 参数
     */
    public static void main(String[] args) {
        String password = "";
//        password = Encrypt.md5("fy123456"+"fy");
//        System.out.println(password);
        password = Encrypt.md5("123456"+"15189475213");
        System.out.println(password);
        password = Encrypt.md5("123456"+"13775821287");
        System.out.println(password);
        password = Encrypt.md5("123456"+"15252185051");
        System.out.println(password);
        password = Encrypt.md5("123456"+"13952142728");
        System.out.println(password);
        password = Encrypt.md5("123456"+"15952251587");
        System.out.println(password);
        password = Encrypt.md5("123456"+"15298794933");
        System.out.println(password);
        password = Encrypt.md5("123456"+"15062021220");
        System.out.println(password);
        password = Encrypt.md5("123456"+"15862252128");
        System.out.println(password);
        password = Encrypt.md5("123456"+"13912022117");
        System.out.println(password);
        password = Encrypt.md5("123456"+"17826255387");
        System.out.println(password);
    }
}

package com.theus.ent.base.service.system.impl;

import com.theus.ent.base.common.constants.RedisKeyPrefix;
import com.theus.ent.base.model.po.system.SysUser;
import com.theus.ent.base.service.system.RedisService;
import com.theus.ent.base.util.RedisUtil;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * 业务数据缓存处理
 *
 * @author tangwei
 * @date 2020-03-17 16:13
 */
@Repository
public class RedisServiceImpl implements RedisService {
    @Resource
    private RedisUtil redisUtil;

    @Override
    public SysUser getUser(String account) {
        String userKey = RedisKeyPrefix.USER + account;
        SysUser sysUser = null;
        try {
            // 获取redis中缓存的用户
            if (redisUtil.existKey(userKey)) {
                sysUser = (SysUser) redisUtil.getObject(userKey);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return sysUser;
    }

    @Override
    public void addUser(String account, SysUser sysUser) {
        String dictKey = RedisKeyPrefix.USER + account;
        // 过期时间设置为3天
        redisUtil.add(dictKey, sysUser, 3, TimeUnit.DAYS);
    }

    @Override
    public void removeUser(String account) {
        String userKey = RedisKeyPrefix.USER + account;
        // 获取redis中缓存的用户
        if (redisUtil.existKey(userKey)) {
            redisUtil.delete(userKey);
        }
    }

}

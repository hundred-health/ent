package com.theus.ent.base.controller.system;

import com.theus.ent.base.common.annotation.SysLogs;
import com.theus.ent.base.model.dto.system.relation.FindUserRelationDTO;
import com.theus.ent.base.model.dto.system.relation.UserRelationAddDTO;
import com.theus.ent.base.model.dto.system.relation.UserRelationDTO;
import com.theus.ent.base.model.dto.system.user.UserAddDTO;
import com.theus.ent.base.service.system.UserRelationService;
import com.theus.ent.core.bean.ResponseCode;
import com.theus.ent.core.bean.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户关系控制器
 *
 * @author tangwei
 * @date 2021-07-13 10:41
 */
@Api(tags = {"用户关系管理"})
@RestController
@RequestMapping("/system/user/relation")
public class UserRelationController {

    @Resource
    private UserRelationService userRelationService;

    @PostMapping(value = {"/unselected"})
    @ApiOperation(value = "获取未选择的用户")
    @SysLogs("获取未选择的用户")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<List<UserRelationDTO>> queryUnselected(@RequestBody @Validated @ApiParam(value = "查询条件") FindUserRelationDTO findUserRelationDTO) {
        return ResponseResult.e(ResponseCode.OK, userRelationService.queryUnselected(findUserRelationDTO));
    }

    @PostMapping(value = {"/selected"})
    @ApiOperation(value = "获取已选择的用户")
    @SysLogs("获取已选择的用户")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<List<UserRelationDTO>> querySelected(@RequestBody @Validated @ApiParam(value = "查询条件") FindUserRelationDTO findUserRelationDTO) {
        return ResponseResult.e(ResponseCode.OK, userRelationService.querySelected(findUserRelationDTO));
    }

    @PostMapping(value = {"/remove/{code}"})
    @ApiOperation(value = "删除已选择的用户关系")
    @SysLogs("删除已选择的用户关系")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<String> remove(@PathVariable("code") @ApiParam(value = "用户标识ID") String code) {
        userRelationService.removeUserRelation(code);
        return ResponseResult.e(ResponseCode.OK);
    }

    @PostMapping(value = {"/add"})
    @ApiOperation(value = "添加用户关系")
    @SysLogs("添加用户关系")
    @ApiImplicitParam(paramType = "header", name = "Authorization", value = "身份认证Token")
    public ResponseResult<String> add(@RequestBody @Validated @ApiParam(value = "用户数据") UserRelationAddDTO relationAddDTO) {
        userRelationService.addUserRelation(relationAddDTO);
        return ResponseResult.e(ResponseCode.OK);
    }
}

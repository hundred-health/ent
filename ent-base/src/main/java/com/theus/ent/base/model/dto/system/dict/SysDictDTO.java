package com.theus.ent.base.model.dto.system.dict;

import lombok.Data;

/**
 * @author tangwei
 * @date 2021-02-22 16:48
 */
@Data
public class SysDictDTO {

    private String classCode;

    private String itemName;

    private String itemValue;

    private Integer sort;
}

package com.theu.ent.http.config;

/**
 * @author tangwei
 * @date 2019-09-22 18:30
 */
public interface HttpConstants {

    /**
     * 请求类型
     */
    interface RequestType {
        String GET = "GET";

        String POST = "POST";
    }

}